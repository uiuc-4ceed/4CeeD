4CeeD Curator 
=====

This is a very brief introduction to the 4CeeD Curator component of 4CeeD.

4CeeD Curator is based on [Clowder](https://opensource.ncsa.illinois.edu/bitbucket/projects/CATS/repos/clowder/browse)

## Prerequisites
- JDK (7 or later)
- Docker (to build Docker image, 1.9.1 or later)
- Clowder's dependencies (please refer to [Clowder's README](Clowder_README.md))

## Important sections of the curator.conf file:

Before running the curator, update the configuration information in `curator.conf` file.
- `CLOWDER_ADMINS`: Email addresses of Curator's admins
- `MONGO_URI`: URI to MongoDB server
- `SMTP_HOST`: SMTP server
- `RABBITMQ_URI`: URI to RabbitMQ server
- `ELASTICSEARCH_SERVICE_SERVER`: IP address of ElasticSearch server
- `UPLOADER_HOME`: Home address of this uploader