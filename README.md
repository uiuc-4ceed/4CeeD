4CeeD Framework
====

4CeeD is a framework that supports **C**apture, **C**urate, **C**oordinate, **C**orrelate, and **D**istribute scientific data. For more information.

This repository consists of a collections of deployment scripts for various tools and components that make up 4CeeD.

## 4CeeD In Action

Please watch the following Youtube video to see how scientific users use 4CeeD in real environments.

<a href="https://www.youtube.com/watch?v=ICDqsOGgwg0" target="_blank"><img src="doc/img/4ceed_video_thumbnail.jpg" border="1px" height="300px" align="middle" alt="4CeeD In Action"/></a>

## New Features - 2018/12
- [Updated dashboard/visualization]
- [Mobile view support]
- [Jupyter notebook integration]
- [Support for LDAP Authentication/CILogin]

## Setup
- [docker-compose](doc/howto/docker_setup.md): Setup 4CeeD as a group of Docker containers running on a single machine. The most simple way to setup 4CeeD. Recommended for a quick test of 4CeeD functionalities.

## How to
- [How to: Register extractors](doc/howto/register_extractors.md): How to register extractors in 4CeeD.
- [How to: Run 4CeeD in persistent mode](doc/howto/persistent_mode.md): How to run 4Ceed in persistent mode.
- [How to: Access 4CeeD using a DNS name](doc/howto/domain_name.md): How to access a 4CeeD instance via a DNS name.
- [How to: Upgrade 4CeeD components](doc/howto/upgrade.md): How to upgrade individual 4CeeD components. 
- [How to: Roll back a 4Ceed component](doc/howto/rollback_component.md): How to roll back an individual 4CeeD component.
- [How to: Backup the MongoDB database](doc/howto/mongodb_backup.md): How to backup the MongoDB database that 4CeeD uses.
- [How to: Restore the MongoDB database](doc/howto/mongodb_restore.md): How to restore the MongoDB database that 4CeeD uses.

## Legacy Setup Information
- [minikube](doc/howto/legacy/minikube_setup.md): Recommended for a quick setup of 4CeeD running on Kubernetes environment.
- [Kubernetes](doc/howto/legacy/kubernetes_setup.md): Setup 4CeeD on a Kubernetes cluster. Our recommended way to setup 4CeeD in production.
- [Google Cloud Platform](doc/howto/legacy/gcp_setup.md): Setup 4CeeD on using Google Cloud Platform's Container Engine. 
- [Setup Kubernetes v1.6.6 on Ubuntu 16.04 LTS cluster](doc/howto/legacy/k8s_setup_ubuntu.md): Setup 4Ceed on Ubuntu 16.04 Kubernetes cluster.
- [Curator information and setup](doc/howto/legacy/curator.md): Brief introduction to the 4CeeD Curator component.

## Contact
Feel free to open issues or create pull requests. Please [join 4CeeD's Slack channel](https://join.slack.com/t/4ceed/shared_invite/MjMyMDIyMDc2OTc4LTE1MDM2OTYzODUtNWU3ZWQ5Yzc1OA) if you have any question.
