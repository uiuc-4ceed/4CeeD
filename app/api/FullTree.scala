package api

import java.io.FileInputStream
import java.util.Date

import javax.inject.Inject
import _root_.util.License
import api.Permission._
//import com.wordnik.swagger.annotations.{Api, ApiOperation}
import controllers.routes
import models._
import org.apache.commons.codec.binary.Hex
import play.api.Logger
import play.api.libs.json._
import play.api.libs.json.Json.toJson
import play.api.mvc.Action
import services._

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

//@Api(value = "/api/fulltree", description = "Controller for tree routes.")
class FullTree @Inject()(events: EventService, vocabularies: VocabularyService, vocabularyTermService: VocabularyTermService, datasets: DatasetService, collections: CollectionService) extends ApiController {


  def getChildrenOfNode(nodeId: Option[String], nodeType: String, role: Option[String]) = PrivateServerAction { implicit request =>
    val user = request.user.get
    var children: ListBuffer[JsValue] = ListBuffer.empty[JsValue]

    if (nodeType == "root") {
      var result = toJson(getChildrenOfRoot(user))
      Ok(result)
    } else if (nodeType == "space") {
      nodeId match {
        case Some(id) => {
          spaces.get(UUID(id)) match {
            case Some(space) => {
              var current_role = "viewer"
              if (space.creator.uuid == user.id) {
                current_role = "owner"
              }
              var result = getChildrenOfSpace(space, user, current_role)
              Ok(toJson(result))
            }
            case None => BadRequest("No space found with id")
          }
        }
        case None => BadRequest("No nodeId supplied")
      }
    } else if (nodeType == "collection") {
      nodeId match {
        case Some(id) => {
          collections.get(UUID(id)) match {
            case Some(collection) => {
              var current_role = "viewer"
              if (collection.author.id == user.id) {
                current_role = "owner"
              }
              var result = getChildrenOfCollection(collection, user, current_role)
              Ok(toJson(result))
            }
            case None => {
              BadRequest("No collection found with id")
            }
          }
        }
        case None => {
          BadRequest("No nodeId supplied")
        }
      }

    } else if (nodeType == "dataset") {
      nodeId match {
        case Some(id) => {
          datasets.get(UUID(id)) match {
            case Some(ds) => {
              var current_role = "viewer"
              if (ds.author.id == user.id) {
                current_role = "owner"
              }
              var result = getChildrenOfDataset(ds, Some(user), current_role)
              Ok(toJson(result))
            }
            case None => {
              BadRequest("No dataset found with id")
            }
          }
        }
        case None => {
          BadRequest("No id supplied")
        }
      }

    } else {
      Ok(toJson("not implemented"))
    }
  }


  def getChildrenOfRoot(user: User): List[JsValue] = {
    var children: ListBuffer[JsValue] = ListBuffer.empty[JsValue];
    var mySpaceList = spaces.listUser(0, Some(user), false, user)
    var viewSpaceList = spaces.listAccess(0, Set[Permission](Permission.ViewSpace), Some(user), false, false, false, true).filter((s: ProjectSpace) => (s.creator != user.id))

    for (space <- mySpaceList) {
      var role = "owner"
      var numDatasetsOfSpace = spaces.getDatasetsInSpace(Some(space.id.stringify)).size
      var numCollectionsOfSpace = spaces.getCollectionsInSpace(Some(space.id.stringify)).size

      if (numCollectionsOfSpace == 0 && numDatasetsOfSpace == 0) {
        var currentJson = Json.obj("id" -> space.id, "value" -> space.name, "type" -> "space", "webix_kids" -> false, "icon" -> "home", "role" -> role)
        children += currentJson
      } else {
        var currentJson = Json.obj("id" -> space.id, "value" -> space.name, "type" -> "space", "webix_kids" -> true, "icon" -> "home", "role" -> role)
        children += currentJson
      }


    }

    for (space <- viewSpaceList) {
      var numDatasetsOfSpace = spaces.getDatasetsInSpace(Some(space.id.stringify)).size
      var numCollectionsOfSpace = spaces.getCollectionsInSpace(Some(space.id.stringify)).size

      if (numCollectionsOfSpace == 0 && numDatasetsOfSpace == 0) {
        var currentJson = Json.obj("id" -> space.id, "value" -> space.name, "type" -> "space", "webix_kids" -> false, "icon" -> "home", "role" -> "viewer")
        children += currentJson
      } else {
        var currentJson = Json.obj("id" -> space.id, "value" -> space.name, "type" -> "space", "webix_kids" -> true, "icon" -> "home", "role" -> "viewer")
        children += currentJson
      }
    }

    var orphanCollectionsNotInSpace = getOrphanCollectionsNotInSpace(user)


    for (col <- orphanCollectionsNotInSpace) {
      var hasChildren = false
      if (!col.child_collection_ids.isEmpty || col.datasetCount > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> col.thumbnail_id)
      var currentJson = Json.obj("id" -> col.id, "value" -> col.name, "type" -> "collection", "webix_kids" -> hasChildren, "icon" -> "puzzle", "role" -> "owner")
      children += currentJson
    }

    var orphanDatasetsNotInSpace = getOrphanDatasetsNotInSpace(user)

    var orphanDatasetsPublic = getOrphanDatasetsPublic(user, false).filter((d: Dataset) => (d.author.id != user.id))

    var orphanDatasetsMine = getMyOrphanDatasetsPublic(user)

    for (ds <- orphanDatasetsNotInSpace) {
      var hasChildren = false
      if (ds.files.size > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> ds.thumbnail_id)
      var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder",  "role" -> "owner")
      children += currentJson
    }

    for (ds <- orphanDatasetsPublic) {
      var hasChildren = false
      if (ds.files.size > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> ds.thumbnail_id)
      var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder", "role" -> "viewer")
      children += currentJson
    }

    for (ds <- orphanDatasetsMine) {
      var hasChildren = false
      if (ds.files.size > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> ds.thumbnail_id)
      var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren,  "icon" -> "folder", "role" -> "owner")
      children += currentJson
    }

    return children.toList

  }


  def getChildrenOfSpace(space: ProjectSpace, user: User, roleInSpace: String): List[JsValue] = {
    var children: ListBuffer[JsValue] = ListBuffer.empty[JsValue]

    var collectionsInSpace = collections.listSpace(0, space.id.stringify)
    for (col <- collectionsInSpace) {
      var hasChildren = false
      if (!col.child_collection_ids.isEmpty || col.datasetCount > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> col.thumbnail_id)
      if (col.author.id == user.id) {
        var currentJson = Json.obj("id" -> col.id, "value" -> col.name, "type" -> "collection", "icon" -> "puzzle", "webix_kids" -> hasChildren, "role" -> "owner")
        children += currentJson

      } else {
        var currentJson = Json.obj("id" -> col.id, "value" -> col.name, "type" -> "collection",  "icon" -> "puzzle", "webix_kids" -> hasChildren, "role" -> roleInSpace)
        children += currentJson
      }
    }

    var orphanDatasetsInSpace = getOrphanDatasetsInSpace(space, user).filter((d: Dataset) => (d.author.id == user.id))
    for (ds <- orphanDatasetsInSpace) {
      var hasChildren = false
      if (ds.files.size > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> ds.thumbnail_id)
      if (ds.author.id == user.id) {
        var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder", "role" -> "owner")
        children += currentJson
      } else {
        var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder", "role" -> roleInSpace)
        children += currentJson
      }

    }
    children.toList

  }

  def getChildrenOfCollection(collection: Collection, user: User, roleInSpace: String): List[JsValue] = {
    var children: ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, Some(user))
    for (ds <- datasetsInCollection) {
      var hasChildren = false
      if (ds.files.size > 0) {
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id" -> ds.thumbnail_id)
      if (ds.author.id == user.id) {
        var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder","role" -> "owner")
        children += currentJson
      } else {
        var currentJson = Json.obj("id" -> ds.id, "value" -> ds.name, "type" -> "dataset", "webix_kids" -> hasChildren, "icon" -> "folder", "role" -> "viewer")
        children += currentJson
      }

    }
    for (child_id <- collection.child_collection_ids) {
      collections.get(child_id) match {
        case Some(child) => {
          if (child.trash == false) {
            var hasChildren = false
            if (!child.child_collection_ids.isEmpty || child.datasetCount > 0) {
              hasChildren = true
            }
            var data = Json.obj("thumbnail_id" -> child.thumbnail_id)
            if (child.author.id == user.id) {
              var currentJson = Json.obj("id" -> child.id, "value" -> child.name, "type" -> "collection", "icon" -> "puzzle", "webix_kids" -> hasChildren, "role" -> "owner")
              children += currentJson
            } else {
              var currentJson = Json.obj("id" -> child.id, "value" -> child.name, "type" -> "collection", "icon" -> "puzzle", "webix_kids" -> hasChildren, "role" -> "viewer")
              children += currentJson
            }
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfDataset(dataset: Dataset, user: Option[User], roleInSpace: String): List[JsValue] = {
    var children: ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    var files_inside = dataset.files
    for (f <- files_inside) {
      files.get(f) match {
        case Some(file) => {
          file.thumbnail_id match {
            case Some(thumbnail) => {
              var data = Json.obj("thumbnail_id" -> file.thumbnail_id)
              var currentJson = Json.obj("id" -> file.id, "value" -> file.filename, "type" -> "file", "icon" -> "xyz", "role" -> roleInSpace)
              children += currentJson
            }
            case None => {
              var data = Json.obj("thumbnail_id" -> file.thumbnail_id)
              var currentJson = Json.obj("id" -> file.id, "value" -> file.filename, "type" -> "file", "icon" -> "xyz", "role" -> roleInSpace)
              children += currentJson
            }
          }
        }
      }
    }
    children.toList
  }

  def getOrphanCollectionsNotInSpace(user: User): List[Collection] = {
    var collectionsNotInSpace = collections.listUser(0, Some(user), false, user).filter((c: Collection) => (c.spaces.isEmpty && c.parent_collection_ids.isEmpty))
    collectionsNotInSpace
  }

  def getOrphanDatasetsNotInSpace(user: User): List[Dataset] = {
    var datasetsNotInSpace = datasets.listUser(0, Some(user), false, user).filter((d: Dataset) => (d.spaces.isEmpty && d.collections.isEmpty && (!d.isPublic)))
    datasetsNotInSpace
  }

  def getMyOrphanDatasetsPublic(user: User): List[Dataset] = {
    var datasetsNotInSpace = datasets.listUser(0, Some(user), false, user).filter((d: Dataset) => (d.spaces.isEmpty && d.collections.isEmpty && (d.isPublic) && (d.author.id == user.id)))
    datasetsNotInSpace
  }

  def getOrphanDatasetsPublic(user: User, mine: Boolean): List[Dataset] = {
    var datasetsPublic = datasets.listAccess(0, Set[Permission](Permission.ViewDataset), Some(user), false, true, false).filter((d: Dataset) => (d.isPublic))
    if (mine) {
      datasetsPublic
    } else {
      datasetsPublic.filter((d: Dataset) => (d.author.id != user.id))
    }
  }

  def getOrphanDatasetsInSpace(space: ProjectSpace, user: User): List[Dataset] = {
    var orphanDatasets: ListBuffer[Dataset] = ListBuffer.empty[Dataset]
    var datasetsInSpace = datasets.listSpace(0, space.id.stringify, Some(user))
    for (dataset <- datasetsInSpace) {
      if (!datasetHasCollectionInSpace(dataset, space)) {
        orphanDatasets += dataset
      }
    }
    orphanDatasets.toList
  }

  def datasetHasCollectionInSpace(dataset: Dataset, space: ProjectSpace): Boolean = {
    var hasCollectionInSpace = false;
    var datasetCollectionIds = dataset.collections
    if (datasetCollectionIds.isEmpty) {
      hasCollectionInSpace = false
      return hasCollectionInSpace
    }
    for (col_id <- datasetCollectionIds) {
      collections.get(col_id) match {
        case Some(col) => {
          if (col.spaces.contains(space.id)) {
            hasCollectionInSpace = true
            return hasCollectionInSpace
          }
        }
      }
    }
    return hasCollectionInSpace
  }

  def datasetPublic(dataset: Dataset): Boolean = {
    var public = false;
    val spacesOfDataset = dataset.spaces
    for (space <- spacesOfDataset) {
      spaces.get(space) match {
        case Some(s) => {
          if (s.isPublic) {
            public = true;
            return public
          }
        }
        case None =>
      }
    }
    return public
  }

  def collectionPublic(collection: Collection): Boolean = {
    var public = false;
    val spacesOfCollection = collection.spaces
    for (space <- spacesOfCollection) {
      spaces.get(space) match {
        case Some(s) => {
          if (s.isPublic) {
            public = true;
            return public
          }
        }
        case None =>
      }
    }
    return public
  }

}

