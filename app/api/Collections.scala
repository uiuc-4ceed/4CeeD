package api

import java.io.{ByteArrayInputStream, InputStream, ByteArrayOutputStream}
import java.security.{DigestInputStream, MessageDigest}
import java.text.SimpleDateFormat
import java.util.zip.{ZipEntry, ZipOutputStream, Deflater}

import Iterators.RootCollectionIterator
import _root_.util.JSONLD
import api.Permission.Permission
import org.apache.commons.codec.binary.Hex
import play.api.Logger
import play.api.Play.current
import models._
import play.api.libs.iteratee.Enumerator
import services._
import play.api.libs.json._
import play.api.libs.json.{JsObject, JsValue}
import play.api.libs.json.Json.toJson
import javax.inject.{ Singleton, Inject}
import scala.collection.mutable.ListBuffer
import scala.concurrent.{Future, ExecutionContext}
import play.api.libs.concurrent.Execution.Implicits._
import scala.util.parsing.json.JSONArray
import scala.util.{Try, Success, Failure}
import java.util.{Calendar, Date}
import controllers.Utils
import java.io.File
import java.io.File.TempDirectory
import java.io._
import java.net.URL
import java.nio._
import java.nio.file.{Paths, Path, Files}
import java.security.{DigestInputStream, MessageDigest}
import java.text.SimpleDateFormat
import java.util
import java.util.zip.{ZipInputStream, ZipEntry, ZipOutputStream, Deflater}
import java.util.Enumeration
import javax.activation.MimetypesFileTypeMap
import Iterators.RootCollectionIterator
import _root_.util.{FileUtils, JSONLD}
import api.Permission.Permission
import fileutils.FilesUtils
import org.apache.commons.codec.binary.Hex
import play.api.Logger
import play.api.Play.current
import models._
import play.api.libs.iteratee.Enumerator
import services._
import play.api.libs.json._
import play.api.libs.json.{JsObject, JsValue}
import play.api.libs.json.Json.toJson
import javax.inject.{ Singleton, Inject}
import scala.collection.immutable.List
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{Future, ExecutionContext}
import play.api.libs.concurrent.Execution.Implicits._
import scala.util.parsing.json.JSONArray
import scala.util.{Try, Success, Failure}
import java.util.{Calendar, Date}
import controllers.{routes, Utils}
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import scala.collection.immutable.List


/**
 * Manipulate collections.
 */
@Singleton
class Collections @Inject() (datasets: DatasetService,
                             collections: CollectionService,
                             previews: PreviewService,
                             userService: UserService,
                             events: EventService,
                             spaces:SpaceService,
                             appConfig: AppConfigurationService,
                             folders : FolderService,
                             files: FileService,
                             sparql: RdfSPARQLService,
                             dtsrequests: ExtractionRequestsService,
                             metadataService : MetadataService) extends ApiController {

  def createCollection() = PermissionAction(Permission.CreateCollection) (parse.json) { implicit request =>
    Logger.debug("Creating new collection")
    (request.body \ "name").asOpt[String].map { name =>

      var c : Collection = null
      implicit val user = request.user
      user match {
        case Some(identity) => {
          val description = (request.body \ "description").asOpt[String].getOrElse("")
          (request.body \ "space").asOpt[String] match {
            case None | Some("default") => c = Collection(name = name, description = description, created = new Date(), datasetCount = 0, author = identity)
            case Some(space) =>  if (spaces.get(UUID(space)).isDefined) {

              c = Collection(name = name, description = description, created = new Date(), datasetCount = 0, author = identity, spaces = List(UUID(space)))
            } else {
              BadRequest(toJson("Bad space = " + space))
            }
          }

          collections.insert(c) match {
            case Some(id) => {
              appConfig.incrementCount('collections, 1)
              c.spaces.map(spaceId => spaces.get(spaceId)).flatten.map{ s =>
                spaces.addCollection(c.id, s.id, user)
                collections.addToRootSpaces(c.id, s.id)
                events.addSourceEvent(request.user, c.id, c.name, s.id, s.name, "add_collection_space")
              }
              Ok(toJson(Map("id" -> id)))
            }
            case None => Ok(toJson(Map("status" -> "error")))
          }
        }
        case None => InternalServerError("User Not found")
      }
    }.getOrElse(BadRequest(toJson("Missing parameter [name]")))
  }

  def attachDataset(collectionId: UUID, datasetId: UUID) = PermissionAction(Permission.AddResourceToCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) { implicit request =>

    collections.addDataset(collectionId, datasetId) match {
      case Success(_) => {
        var datasetsInCollection = 0
        collections.get(collectionId) match {
          case Some(collection) => {
            datasets.get(datasetId) match {
              case Some(dataset) => {
                if (play.Play.application().configuration().getBoolean("addDatasetToCollectionSpace")){
                  collections.addDatasetToCollectionSpaces(collection.id,dataset.id, request.user)
                }
                events.addSourceEvent(request.user , dataset.id, dataset.name, collection.id, collection.name, "attach_dataset_collection")
              }
              case None =>
            }
            datasetsInCollection = collection.datasetCount
          }
          case None =>
        }
        //datasetsInCollection is the number of datasets in this collection
        Ok(Json.obj("datasetsInCollection" -> Json.toJson(datasetsInCollection) ))
      }
      case Failure(t) => InternalServerError
    }

  }

  /**
   * Reindex the given collection, if recursive is set to true it will
   * also reindex all datasets and files.
   */
  def reindex(id: UUID, recursive: Boolean) = PermissionAction(Permission.CreateCollection, Some(ResourceRef(ResourceRef.collection, id))) {  implicit request =>
      collections.get(id) match {
        case Some(coll) => {
          current.plugin[ElasticsearchPlugin].foreach {
            _.index(coll, recursive)
          }
          Ok(toJson(Map("status" -> "success")))
        }
        case None => {
          Logger.error("Error getting collection" + id)
          BadRequest(toJson(s"The given collection id $id is not a valid ObjectId."))
        }
      }
  }

  def removeDataset(collectionId: UUID, datasetId: UUID, ignoreNotFound: String) = PermissionAction(Permission.RemoveResourceFromCollection, Some(ResourceRef(ResourceRef.collection, collectionId)), Some(ResourceRef(ResourceRef.dataset, datasetId))) { implicit request =>
    collections.removeDataset(collectionId, datasetId, Try(ignoreNotFound.toBoolean).getOrElse(true)) match {
      case Success(_) => {

        collections.get(collectionId) match {
        case Some(collection) => {
          datasets.get(datasetId) match {
            case Some(dataset) => {
              events.addSourceEvent(request.user , dataset.id, dataset.name, collection.id, collection.name, "remove_dataset_collection")
            }
            case None =>
          }
        }
        case None =>
      }
      Ok(toJson(Map("status" -> "success")))
    }
    case Failure(t) => {
      Logger.error("Error: " + t)
      InternalServerError
    }
    }
  }

  

  def removeCollection(collectionId: UUID) = PermissionAction(Permission.DeleteCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) { implicit request =>
    collections.get(collectionId) match {
      case Some(collection) => {
        val useTrash = play.api.Play.configuration.getBoolean("useTrash").getOrElse(false)
        if (!useTrash || (useTrash && collection.trash)){
          events.addObjectEvent(request.user , collection.id, collection.name, "delete_collection")
          collections.delete(collectionId)
          appConfig.incrementCount('collections, -1)
          current.plugin[AdminsNotifierPlugin].foreach {
            _.sendAdminsNotification(Utils.baseUrl(request),"Collection","removed",collection.id.stringify, collection.name)
          }
        } else {
          collections.addToTrash(collectionId, Some(new Date()))
          events.addObjectEvent(request.user, collectionId, collection.name, "move_collection_trash")
          Ok(toJson(Map("status" -> "success")))
        }
      }
    }
    //Success anyway, as if collection is not found it is most probably deleted already
    Ok(toJson(Map("status" -> "success")))
  }

  def restoreCollection(collectionId : UUID) = PermissionAction(Permission.DeleteCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) {implicit request=>
    implicit val user = request.user
    user match {
      case Some(u) => {
        collections.get(collectionId) match {
          case Some(col) => {
            collections.restoreFromTrash(collectionId, None)
            events.addObjectEvent(user, collectionId, col.name, "restore_collection_trash")
            Ok(toJson(Map("status" -> "success")))
          }
          case None => InternalServerError("Update Access failed")
        }
      }
      case None => BadRequest("No user supplied")
    }
  }

  def listCollectionsInTrash(limit : Int) = PrivateServerAction {implicit request =>
    val trash_collections_list = request.user match {
      case Some(usr) => {
        for (collection <- collections.listUserTrash(request.user,limit))
          yield jsonCollection(collection)
      }
      case None => List.empty
    }
    Ok(toJson(trash_collections_list))
  }

  def emptyTrash() = PrivateServerAction {implicit request =>
    val user = request.user
    user match {
      case Some(u) => {
        val trashcollections = collections.listUserTrash(request.user,0)
        for (collection <- trashcollections){
          events.addObjectEvent(request.user , collection.id, collection.name, "delete_collection")
          collections.delete(collection.id)
          appConfig.incrementCount('collections, -1)
          current.plugin[AdminsNotifierPlugin].foreach {
            _.sendAdminsNotification(Utils.baseUrl(request),"Collection","removed",collection.id.stringify, collection.name)
          }
        }
      }
      case None =>
    }
    Ok(toJson("Done emptying trash"))
  }

  def clearOldCollectionsTrash(days : Int) = ServerAdminAction {implicit request =>

    val deleteBeforeCalendar : Calendar = Calendar.getInstance()
    deleteBeforeCalendar.add(Calendar.DATE,-days)

    val deleteBeforeDateTime = deleteBeforeCalendar.getTimeInMillis()
    val user = request.user
    user match {
      case Some(u) => {
        val allCollectionsTrash = collections.listUserTrash(None,0)
        allCollectionsTrash.foreach( c => {
          val dateInTrash = c.dateMovedToTrash.getOrElse(new Date())
          if (dateInTrash.getTime() < deleteBeforeDateTime) {
            events.addObjectEvent(request.user , c.id, c.name, "delete_collection")
            collections.delete(c.id)
            appConfig.incrementCount('collections, -1)
            current.plugin[AdminsNotifierPlugin].foreach {
              _.sendAdminsNotification(Utils.baseUrl(request),"Collection","removed",c.id.stringify, c.name)
            }
          }
        })
        Ok(toJson("Deleted all collections in trash older than " + days + " days"))
      }
      case None => BadRequest("No user supplied")
    }
  }

  def removeCollectionAndContents(collectionId: UUID) = PermissionAction(Permission.DeleteCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) { implicit request =>
    val user = request.user
    user match {
      case Some(u) => {
        collections.get(collectionId) match {
          case Some(col) => {
            //deleteCollectionAndContents(col.id,u.id)
            deleteCollectionAndAllContents(col, u)
          }
          case None => BadRequest("No collection found")
        }

      }
      case None => BadRequest("No user supplied")
    }
    Ok(toJson("deleted"))
  }


  def moveDatasetToNewCollection(oldCollection: String, dataset: String, newCollection: String) = PermissionAction(Permission.EditDataset, Some(ResourceRef(ResourceRef.dataset, UUID(dataset)))) { implicit request =>

    if (oldCollection == "" || oldCollection == "#"){
      datasets.get(UUID(dataset)) match {
        case Some(ds) => {
          collections.get(UUID(newCollection)) match {
            case Some(newCol) => {
              collections.addDataset(newCol.id,ds.id)
              Ok(toJson(Map("status" -> "success")))
            }
          }
        }
        case None => BadRequest("No dataset provided")
      }
    }

    var oldCollectionId = UUID(oldCollection)
    var datasetId = UUID(dataset)
    var newCollectionId = UUID(newCollection)

    collections.removeDataset(oldCollectionId, datasetId, true) match {
      case Success(_) => {

        collections.get(oldCollectionId) match {
          case Some(collection) => {
            datasets.get(datasetId) match {
              case Some(dataset) => {
                events.addSourceEvent(request.user , dataset.id, dataset.name, collection.id, collection.name, "remove_dataset_collection")
                collections.get(newCollectionId) match {
                  case Some(newCollection) => {
                    collections.addDataset(newCollectionId,dataset.id)
                  }
                  case None =>
                }
              }
              case None =>
            }
          }
          case None =>
        }
        Ok(toJson(Map("status" -> "success")))
      }
      case Failure(t) => {
        Logger.error("Error: " + t)
        InternalServerError
      }
    }
  }

  def moveChildCollection(id : UUID) = PermissionAction(Permission.EditCollection,Some(ResourceRef(ResourceRef.collection, id))) (parse.json) { implicit request =>
    val user = request.user
    val originalCollection = (request.body \ "originalCollection").asOpt[String].getOrElse("")
    val newCollection = (request.body \ "newCollection").asOpt[String].getOrElse("")
    user match {
      case Some(u) => {
        collections.get(id) match {
          case Some(collection) => {
            if (originalCollection == "" || originalCollection == "#"){
              collections.get(UUID(newCollection)) match {
                case Some(newCol) => {
                  if (collection.parent_collection_ids.contains(newCol.id)){
                    Ok(toJson(Map("status"->"success")))
                  } else {
                    collections.addSubCollection(newCol.id,collection.id,user)
                    Ok(toJson(Map("status"->"success")))
                  }
                }
                case None => BadRequest("No collections")
              }
            }
            collections.get(UUID(originalCollection)) match {
              case Some(originalCol) =>{
                if (collection.parent_collection_ids.contains(originalCol.id)){
                  collections.get(UUID(newCollection)) match {
                    case Some(newCol) => {
                      if (collection.parent_collection_ids.contains(newCol.id)){
                        collections.removeSubCollection(originalCol.id,collection.id)
                        Ok(toJson(Map("status"->"success")))
                      } else {
                        collections.removeSubCollection(originalCol.id,collection.id)
                        collections.addSubCollection(newCol.id,collection.id,user)
                        Ok(toJson(Map("status"->"success")))
                      }
                    }
                    case None => BadRequest("No collection matches id " + newCollection)
                  }
                }
                else {
                  BadRequest("Dataset is not in collection " + originalCollection)
                }
              }
              case None => BadRequest("No collection matches id " + originalCollection)
            }
          }
          case None => BadRequest("No dataset found for id " + id)
        }
      }
      case None => BadRequest("No user supplied")
    }
  }



  def list(title: Option[String], date: Option[String], limit: Int, exact: Boolean) = PrivateServerAction { implicit request =>
    Ok(toJson(listCollections(title, date, limit, Set[Permission](Permission.ViewCollection), false, request.user, request.user.fold(false)(_.superAdminMode), exact)))
  }

  def listCanEdit(title: Option[String], date: Option[String], limit: Int, exact: Boolean) = PrivateServerAction { implicit request =>
    Ok(toJson(listCollections(title, date, limit, Set[Permission](Permission.AddResourceToCollection, Permission.EditCollection), false, request.user, request.user.fold(false)(_.superAdminMode), exact)))
  }

  def addDatasetToCollectionOptions(datasetId: UUID, title: Option[String], date: Option[String], limit: Int, exact: Boolean) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    var listAll = false
    var collectionList: List[Collection] = List.empty
    if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined) {
      listAll = true
    } else {
      datasets.get(datasetId) match {
        case Some(dataset) => {
          if(dataset.spaces.length > 0) {
            collectionList = collections.listInSpaceList(title, date, limit, dataset.spaces, Set[Permission](Permission.AddResourceToCollection, Permission.EditCollection), user)
          } else {
            listAll = true
          }

        }
        case None => Logger.debug("The dataset was not found")
      }
    }
    if(listAll) {
      collectionList = listCollections(title, date, limit, Set[Permission](Permission.AddResourceToCollection, Permission.EditCollection), false, request.user, request.user.fold(false)(_.superAdminMode), exact)
    }
    Ok(toJson(collectionList))
  }

  private def getNextLevelCollections(current_collections : List[Collection]) : List[Collection] = {
    val next_level_collections : ListBuffer[Collection] = ListBuffer.empty[Collection]
    for (current_collection : Collection <- current_collections){
      for (child_id <- current_collection.child_collection_ids){
        collections.get(child_id) match {
          case Some(child_col) => next_level_collections += child_col
          case None =>
        }
      }
    }
    next_level_collections.toList
  }



  def listPossibleParents(currentCollectionId : String, title: Option[String], date: Option[String], limit: Int, exact: Boolean) = PrivateServerAction { implicit request =>
    val selfAndAncestors = collections.getSelfAndAncestors(UUID(currentCollectionId))
    val descendants = collections.getAllDescendants(UUID(currentCollectionId)).toList
    val allCollections = listCollections(title, date, limit, Set[Permission](Permission.AddResourceToCollection, Permission.EditCollection), false,
      request.user, request.user.fold(false)(_.superAdminMode), exact)
    val possibleNewParents = allCollections.filter((c: Collection) =>
      if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined) {
        (!selfAndAncestors.contains(c) && !descendants.contains(c))
      } else {
            collections.get(UUID(currentCollectionId)) match {
              case Some(coll) => {
                if(coll.spaces.length == 0) {
                   (!selfAndAncestors.contains(c) && !descendants.contains(c))

                } else {
                   (!selfAndAncestors.contains(c) && !descendants.contains(c) && c.spaces.intersect(coll.spaces).length > 0)
                }
              }
              case None => (!selfAndAncestors.contains(c) && !descendants.contains(c))
            }
      }
    )
    Ok(toJson(possibleNewParents))
  }

  def uploadZipToSpace(spaceId : Option[String]) = PermissionAction(Permission.AddFile)(parse.multipartFormData) { implicit request =>
    implicit val user = request.user
    var createdCollections : ListBuffer[UUID] = ListBuffer.empty[UUID]
    var collectionId : Option[UUID] = None
    var uploadSpace : String = ""
    user match {
      case Some(identity) => {
        spaceId match {
          case Some(s) => {
            Logger.info("got the space")
            uploadSpace = s
          }
          case None =>
        }
        request.body.files.map{ f =>
          var nameOfFile = f.filename
          var flags = ""
          if(nameOfFile.toLowerCase().endsWith(".ptm")){
            val thirdSeparatorIndex = nameOfFile.indexOf("__")
            if(thirdSeparatorIndex >= 0){
              val firstSeparatorIndex = nameOfFile.indexOf("_")
              val secondSeparatorIndex = nameOfFile.indexOf("_", firstSeparatorIndex+1)
              flags = flags + "+numberofIterations_" +  nameOfFile.substring(0,firstSeparatorIndex) + "+heightFactor_" + nameOfFile.substring(firstSeparatorIndex+1,secondSeparatorIndex)+ "+ptm3dDetail_" + nameOfFile.substring(secondSeparatorIndex+1,thirdSeparatorIndex)
              nameOfFile = nameOfFile.substring(thirdSeparatorIndex+2)
            }
          }
          Logger.debug("Uploading file " + nameOfFile)

          val showPreviews = "datasetLevel"

          // store file
          val file = files.save(new FileInputStream(f.ref.file), nameOfFile, f.contentType, identity, showPreviews)

          val uploadedFile = f
          file match {
            case Some(f) => {
              val option_user = userService.findByIdentity(identity)
              events.addObjectEvent(option_user, f.id, f.filename, "upload_file")
              if (showPreviews.equals("FileLevel"))
                flags = flags + "+filelevelshowpreviews"
              else if (showPreviews.equals("None"))
                flags = flags + "+nopreviews"
              var fileType = f.contentType
              if (fileType.contains("/zip") || fileType.contains("/x-zip") || nameOfFile.toLowerCase().endsWith(".zip")) {
                fileType = FilesUtils.getMainFileTypeOfZipFile(uploadedFile.ref.file, nameOfFile, "file")
                if (fileType.startsWith("ERROR: ")) {
                  Logger.error(fileType.substring(7))
                  InternalServerError(fileType.substring(7))
                }
                if (fileType.equals("imageset/ptmimages-zipped") || fileType.equals("imageset/ptmimages+zipped") || fileType.equals("multi/files-ptm-zipped")) {
                  if (fileType.equals("multi/files-ptm-zipped")) {
                    fileType = "multi/files-zipped";
                  }

                  val thirdSeparatorIndex = nameOfFile.indexOf("__")
                  if (thirdSeparatorIndex >= 0) {
                    val firstSeparatorIndex = nameOfFile.indexOf("_")
                    val secondSeparatorIndex = nameOfFile.indexOf("_", firstSeparatorIndex + 1)
                    flags = flags + "+numberofIterations_" + nameOfFile.substring(0, firstSeparatorIndex) + "+heightFactor_" + nameOfFile.substring(firstSeparatorIndex + 1, secondSeparatorIndex) + "+ptm3dDetail_" + nameOfFile.substring(secondSeparatorIndex + 1, thirdSeparatorIndex)
                    nameOfFile = nameOfFile.substring(thirdSeparatorIndex + 2)
                    files.renameFile(f.id, nameOfFile)
                  }
                  files.setContentType(f.id, fileType)
                }
              }

              current.plugin[FileDumpService].foreach {
                _.dump(DumpOfFile(uploadedFile.ref.file, f.id.toString, nameOfFile))
              }

              // TODO RK need to replace unknown with the server name
              val key = "unknown." + "file." + fileType.replace(".", "_").replace("/", ".")

              val host = Utils.baseUrl(request)
              val id = f.id

              /** *** Inserting DTS Requests   **/

              val clientIP = request.remoteAddress
              //val clientIP=request.headers.get("Origin").get
              val domain = request.domain
              val keysHeader = request.headers.keys
              //request.
              Logger.debug("---\n \n")

              Logger.debug("clientIP:" + clientIP + "   domain:= " + domain + "  keysHeader=" + keysHeader.toString + "\n")
              Logger.debug("Origin: " + request.headers.get("Origin") + "  Referer=" + request.headers.get("Referer") + " Connections=" + request.headers.get("Connection") + "\n \n")

              Logger.debug("----")
              val serverIP = request.host
              val extra = Map("filename" -> f.filename)
              dtsrequests.insertRequest(serverIP, clientIP, f.filename, id, fileType, f.length, f.uploadDate)

              /** **************************/
              // TODO replace null with None
              current.plugin[RabbitmqPlugin].foreach {
                _.extract(ExtractorMessage(id, id, host, key, extra, f.length.toString, null, flags))
              }

              val dateFormat = new SimpleDateFormat("dd/MM/yyyy")


              //for metadata files
              if (fileType.equals("application/xml") || fileType.equals("text/xml")) {
                val xmlToJSON = FilesUtils.readXMLgetJSON(uploadedFile.ref.file)
                files.addXMLMetadata(id, xmlToJSON)

                Logger.debug("xmlmd=" + xmlToJSON)

                current.plugin[ElasticsearchPlugin].foreach {
                  _.index(f)
                }
              }
              else {
                current.plugin[ElasticsearchPlugin].foreach {
                  _.index(f)
                }
              }

              current.plugin[VersusPlugin].foreach {
                _.indexFile(f.id, fileType)
              }

              //add file to RDF triple store if triple store is used
              if (fileType.equals("application/xml") || fileType.equals("text/xml")) {
                play.api.Play.configuration.getString("userdfSPARQLStore").getOrElse("no") match {
                  case "yes" => sparql.addFileToGraph(f.id)
                  case _ => {}
                }
              }

              current.plugin[AdminsNotifierPlugin].foreach {
                _.sendAdminsNotification(Utils.baseUrl(request), "File", "added", f.id.stringify, nameOfFile)
              }

              val clowderurl = Utils.baseUrl(request)
              val creator_url = api.routes.Users.findById(identity.id).absoluteURL(Utils.https(request))(request)

              //CREATE COLLECTION HERE
              submitZipFileToExtractor(f.id,host,clientIP,serverIP,identity.id.stringify,spaceId)
              //collectionId = createFromZip(f.id, user, clowderurl, creator_url)
              //createdCollections  += collectionId.get

              //Correctly set the updated URLs and data that is needed for the interface to correctly
              //update the display after a successful upload.
              val https = controllers.Utils.https(request)

            }
          }

          Logger.info("We have a file")
        }
      }
      case None =>BadRequest("No user supplied")
    }

    Ok(toJson(createdCollections.toList))
  }

  def submitZipFileToExtractor(file_id: UUID, host : String, clientIP : String, serverIP : String, userId : String, spaceId : Option[String]) =  {
    //Logger.debug(s"Submitting file for extraction with body $request.body")
    // send file to rabbitmq for processing
    current.plugin[RabbitmqPlugin] match {
      case Some(p) =>
        files.get(file_id) match {
          case Some(file) => {
            val id = file.id
            val fileType = file.contentType
            val idAndFlags = ""

            val key = "extractors.zip.collection.create"

            val parameters = if (spaceId == None){
              Json.obj("loggedInUser"->userId)
            } else {
              Json.obj("loggedInUser"->userId,"space"->spaceId.get)
            }

            // if extractor_id is not specified default to execution of all extractors matching mime type
            /*
            val key = (request.body \ "extractor").asOpt[String] match {
              case Some(extractorId) => "extractors." + extractorId
              case None => "unknown." + "file." + fileType.replace(".", "_").replace("/", ".")
            }
            // parameters for execution
            val parameters = (request.body \ "parameters").asOpt[JsObject].getOrElse(JsObject(Seq.empty[(String, JsValue)]))
            */
            // Log request
            dtsrequests.insertRequest(serverIP, clientIP, file.filename, id, fileType, file.length, file.uploadDate)

            val extra = Map("filename" -> file.filename,
              "parameters" -> parameters.toString,
              "action" -> "manual-submission")
            val showPreviews = file.showPreviews

            val newFlags = if (showPreviews.equals("FileLevel"))
              idAndFlags + "+filelevelshowpreviews"
            else if (showPreviews.equals("None"))
              idAndFlags + "+nopreviews"
            else
              idAndFlags

            val originalId = if (!file.isIntermediate) {
              file.id.toString()
            } else {
              idAndFlags
            }

            p.extract(ExtractorMessage(new UUID(originalId), file.id, host, key, extra, file.length.toString, null, newFlags))
            //TODO - add original zip file to the collection that is created
            Ok(Json.obj("status" -> "OK"))
          }
          case None =>
            BadRequest(toJson(Map("request" -> "File not found")))
        }
      case None =>
        Ok(Json.obj("status" -> "error", "msg"-> "RabbitmqPlugin disabled"))
    }
  }


  /**
   * Returns list of collections based on parameters and permissions.
   * TODO this needs to be cleaned up when do permissions for adding to a resource
   */
  private def listCollections(title: Option[String], date: Option[String], limit: Int, permission: Set[Permission], mine: Boolean, user: Option[User], superAdmin: Boolean, exact: Boolean) : List[Collection] = {
    if (mine && user.isEmpty) return List.empty[Collection]

    (title, date) match {
      case (Some(t), Some(d)) => {
        if (mine)
          collections.listUser(d, true, limit, t, user, superAdmin, user.get, exact)
        else
          collections.listAccess(d, true, limit, t, permission, user, superAdmin, true,false, exact)
      }
      case (Some(t), None) => {
        if (mine)
          collections.listUser(limit, t, user, superAdmin, user.get, exact)
        else
          collections.listAccess(limit, t, permission, user, superAdmin, true,false, exact)
      }
      case (None, Some(d)) => {
        if (mine)
          collections.listUser(d, true, limit, user, superAdmin, user.get)
        else
          collections.listAccess(d, true, limit, permission, user, superAdmin, true,false)
      }
       case (None, None) => {
        if (mine)
          collections.listUser(limit, user, superAdmin, user.get)
        else
          collections.listAccess(limit, permission, user, superAdmin, true,false)
      }
    }
  }

  def getCollection(collectionId: UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) { implicit request =>
    collections.get(collectionId) match {
      case Some(x) => Ok(jsonCollection(x))
      case None => BadRequest(toJson("collection not found"))
    }
  }

  def jsonCollection(collection: Collection): JsValue = {
    toJson(Map("id" -> collection.id.toString, "name" -> collection.name, "description" -> collection.description,
      "created" -> collection.created.toString,"author"-> collection.author.toString, "root_flag" -> collections.hasRoot(collection).toString,
      "child_collection_ids"-> collection.child_collection_ids.toString, "parent_collection_ids" -> collection.parent_collection_ids.toString,
    "childCollectionsCount" -> collection.childCollectionsCount.toString, "datasetCount"-> collection.datasetCount.toString, "spaces" -> collection.spaces.toString))
  }

  def updateCollectionName(id: UUID) = PermissionAction(Permission.EditCollection, Some(ResourceRef(ResourceRef.collection, id)))(parse.json) {
    implicit request =>
      implicit val user = request.user
      if (UUID.isValid(id.stringify)) {
        var name: String = null
        val aResult = (request.body \ "name").validate[String]
        aResult match {
          case s: JsSuccess[String] => {
            name = s.get
          }
          case e: JsError => {
            Logger.error("Errors: " + JsError.toFlatJson(e).toString())
            BadRequest(toJson(s"name data is missing"))
          }
        }
        Logger.debug(s"Update title for collection with id $id. New name: $name")
        collections.updateName(id, name)
        collections.get(id) match {
          case Some(collection) => {
            events.addObjectEvent(user, id, collection.name, "update_collection_information")
          }

        }
        collections.index(Some(id))
        Ok(Json.obj("status" -> "success"))
      }
      else {
        Logger.error(s"The given id $id is not a valid ObjectId.")
        BadRequest(toJson(s"The given id $id is not a valid ObjectId."))
      }
  }

  def updateCollectionDescription(id: UUID) = PermissionAction(Permission.EditCollection, Some(ResourceRef(ResourceRef.collection, id)))(parse.json) {
    implicit request =>
      implicit val user = request.user
      if (UUID.isValid(id.stringify)) {
        var description: String = null
        val aResult = (request.body \ "description").validate[String]
        aResult match {
          case s: JsSuccess[String] => {
            description = s.get
          }
          case e: JsError => {
            Logger.error("Errors: " + JsError.toFlatJson(e).toString())
            BadRequest(toJson(s"description data is missing"))
          }
        }
        Logger.debug(s"Update description for collection with id $id. New description: $description")
        collections.updateDescription(id, description)
        collections.get(id) match {
          case Some(collection) => {
            events.addObjectEvent(user, id, collection.name, "update_collection_information")
          }

        }
        collections.index(Some(id))
        Ok(Json.obj("status" -> "success"))
      }
      else {
        Logger.error(s"The given id $id is not a valid ObjectId.")
        BadRequest(toJson(s"The given id $id is not a valid ObjectId."))
      }

  }
  /**
   * Add preview to file.
   */
  def attachPreview(collection_id: UUID, preview_id: UUID) = PermissionAction(Permission.EditCollection, Some(ResourceRef(ResourceRef.collection, collection_id)))(parse.json) { implicit request =>
      // Use the "extractor_id" field contained in the POST data.  Use "Other" if absent.
      val eid = (request.body \ "extractor_id").asOpt[String]
      val extractor_id = if (eid.isDefined) {
        eid
      } else {
        Logger.debug("api.Files.attachPreview(): No \"extractor_id\" specified in request, set it to None.  request.body: " + request.body.toString)
        Some("Other")
      }
      val preview_type = (request.body \ "preview_type").asOpt[String].getOrElse("")
      request.body match {
        case JsObject(fields) => {
          collections.get(collection_id) match {
            case Some(collection) => {
              previews.get(preview_id) match {
                case Some(preview) =>
                  // "extractor_id" is stored at the top level of "Preview".  Remove it from the "metadata" field to avoid dup.
                  // TODO replace null with None
                  previews.attachToCollection(preview_id, collection_id, preview_type, extractor_id, request.body)
                  Ok(toJson(Map("status" -> "success")))
                case None => BadRequest(toJson("Preview not found"))
              }
            }
            //If file to be previewed is not found, just delete the preview
            case None => {
              previews.get(preview_id) match {
                case Some(preview) =>
                  Logger.debug("Collection not found. Deleting previews.files " + preview_id)
                  previews.removePreview(preview)
                  BadRequest(toJson("Collection not found. Preview deleted."))
                case None => BadRequest(toJson("Preview not found"))
              }
            }
          }
        }
        case _ => Ok("received something else: " + request.body + '\n')
      }
  }

  def follow(id: UUID) = AuthenticatedAction { implicit request =>
      implicit val user = request.user

      user match {
        case Some(loggedInUser) => {
          collections.get(id) match {
            case Some(collection) => {
              events.addObjectEvent(user, id, collection.name, "follow_collection")
              collections.addFollower(id, loggedInUser.id)
              userService.followCollection(loggedInUser.id, id)

              val recommendations = getTopRecommendations(id, loggedInUser)
              recommendations match {
                case x::xs => Ok(Json.obj("status" -> "success", "recommendations" -> recommendations))
                case Nil => Ok(Json.obj("status" -> "success"))
              }
            }
            case None => {
              NotFound
            }
          }
        }
        case None => {
          Unauthorized
        }
      }
  }

  def unfollow(id: UUID) = AuthenticatedAction { implicit request =>
      implicit val user = request.user

      user match {
        case Some(loggedInUser) => {
          collections.get(id) match {
            case Some(collection) => {
              events.addObjectEvent(user, id, collection.name, "unfollow_collection")
              collections.removeFollower(id, loggedInUser.id)
              userService.unfollowCollection(loggedInUser.id, id)
              Ok
            }
            case None => {
              NotFound
            }
          }
        }
        case None => {
          Unauthorized
        }
      }
  }

  def getTopRecommendations(followeeUUID: UUID, follower: User): List[MiniEntity] = {
    val followeeModel = collections.get(followeeUUID)
    followeeModel match {
      case Some(followeeModel) => {
        val sourceFollowerIDs = followeeModel.followers
        val excludeIDs = follower.followedEntities.map(typedId => typedId.id) ::: List(followeeUUID, follower.id)
        val num = play.api.Play.configuration.getInt("number_of_recommendations").getOrElse(10)
        userService.getTopRecommendations(sourceFollowerIDs, excludeIDs, num)
      }
      case None => {
        List.empty
      }
    }
  }


  def attachSubCollection(collectionId: UUID, subCollectionId: UUID) = PermissionAction(Permission.AddResourceToCollection, Some(ResourceRef(ResourceRef.collection, collectionId))) { implicit request =>
    collections.addSubCollection(collectionId, subCollectionId, request.user) match {
      case Success(_) => {
        collections.get(collectionId) match {
          case Some(collection) => {
            collections.get(subCollectionId) match {
              case Some(sub_collection) => {
                events.addSourceEvent(request.user, sub_collection.id, sub_collection.name, collection.id, collection.name, "add_sub_collection")
                Ok(jsonCollection(collection))
              }
            }
          }
        }
      }
      case Failure(t) => InternalServerError
    }
  }



  def createCollectionWithParent() = PermissionAction(Permission.CreateCollection) (parse.json) { implicit request =>
    (request.body \ "name").asOpt[String].map{ name =>
      var c : Collection = null
      implicit val user = request.user

      user match {
        case Some(identity) => {
          val description = (request.body \ "description").asOpt[String].getOrElse("")
          (request.body \ "space").asOpt[String] match {
            case None | Some("default") =>  c = Collection(name = name, description = description, created = new Date(), datasetCount = 0, childCollectionsCount = 0, author = identity, root_spaces = List.empty)
            case Some(space) => if (spaces.get(UUID(space)).isDefined) {
              c = Collection(name = name, description = description, created = new Date(), datasetCount = 0, author = identity, spaces = List(UUID(space)), root_spaces = List(UUID(space)))
            } else {
              BadRequest(toJson("Bad space = " + space))
            }
          }

          collections.insert(c) match {
            case Some(id) => {
              appConfig.incrementCount('collections, 1)
              c.spaces.map{ spaceId =>
                spaces.get(spaceId)}.flatten.map{ s =>
                  spaces.addCollection(c.id, s.id, request.user)
                  collections.addToRootSpaces(c.id, s.id)
                  events.addSourceEvent(request.user, c.id, c.name, s.id, s.name, "add_collection_space")
              }

              //do stuff with parent here
              (request.body \"parentId").asOpt[String] match {
                case Some(parentId) => {
                  collections.get(UUID(parentId)) match {
                    case Some(parentCollection) => {
                      collections.addSubCollection(UUID(parentId), UUID(id), user) match {
                        case Success(_) => {
                          Ok(toJson(Map("id" -> id)))
                        }
                      }
                    }
                    case None => {
                      Ok(toJson("No collection with parentId found"))
                    }
                  }
                }
                case None => {
                  Ok(toJson("No parentId supplied"))
                }

              }
              Ok(toJson(Map("id" -> id)))
            }
            case None => Ok(toJson(Map("status" -> "error")))
          }
        }
        case None => InternalServerError("User Not found")
      }

    }.getOrElse(BadRequest(toJson("Missing parameter [name]")))
  }

  def removeSubCollection(collectionId: UUID, subCollectionId: UUID, ignoreNotFound: String) = PermissionAction(Permission.RemoveResourceFromCollection, Some(ResourceRef(ResourceRef.collection, collectionId)), Some(ResourceRef(ResourceRef.collection, subCollectionId))) { implicit request =>

    collections.removeSubCollection(collectionId, subCollectionId, Try(ignoreNotFound.toBoolean).getOrElse(true)) match {
      case Success(_) => {

        collections.get(collectionId) match {
          case Some(collection) => {
            collections.get(subCollectionId) match {
              case Some(sub_collection) => {
                events.addSourceEvent(request.user, sub_collection.id, sub_collection.name, collection.id, collection.name, "remove_subcollection")
              }
            }
          }
        }
        Ok(toJson(Map("status" -> "success")))
      }
      case Failure(t) => InternalServerError
    }
  }

  def isCollectionRootOrHasNoParent(collectionId: UUID): Unit = {
    collections.get(collectionId) match {
      case Some(collection) => {
        if (collections.hasRoot(collection) || collection.parent_collection_ids.isEmpty) {
          return true
        } else
          return false

      }
      case None =>
        Ok("no collection with id : " + collectionId)
    }
  }


  /**
    * Adds a Root flag for a collection in a space
    */
  def setRootSpace(collectionId: UUID, spaceId: UUID)  = PermissionAction(Permission.AddResourceToSpace, Some(ResourceRef(ResourceRef.space, spaceId))) { implicit request =>
    Logger.debug("changing the value of the root flag")
    (collections.get(collectionId), spaces.get(spaceId)) match {
      case (Some(collection), Some(space)) => {
        spaces.addCollection(collectionId, spaceId, request.user)
        collections.addToRootSpaces(collectionId, spaceId)
        events.addSourceEvent(request.user, collection.id, collection.name, space.id, space.name, "add_collection_space")
        Ok(jsonCollection(collection))
      } case (None, _) => {
        Logger.error("Error getting collection  " + collectionId)
        BadRequest(toJson(s"The given collection id $collectionId is not a valid ObjectId."))
      }
      case _ => {
        Logger.error("Error getting space  " + spaceId)
        BadRequest(toJson(s"The given space id $spaceId is not a valid ObjectId."))
      }
    }
  }

  /**
    * Remove root flag from a collection in a space
    */
  def unsetRootSpace(collectionId: UUID, spaceId: UUID)  = PermissionAction(Permission.AddResourceToSpace, Some(ResourceRef(ResourceRef.space, spaceId))) { implicit request =>
    Logger.debug("changing the value of the root flag")
    collections.get(collectionId) match {
      case Some(collection) => {
        collections.removeFromRootSpaces(collectionId, spaceId)
        Ok(jsonCollection(collection))
      } case None => {
        Logger.error("Error getting collection  " + collectionId)
        BadRequest(toJson(s"The given collection id $collectionId is not a valid ObjectId."))
      }
    }
  }

  def getRootCollections() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val root_collections_list = for (collection <- collections.listAccess(100,Set[Permission](Permission.ViewCollection),request.user,true, true,false); if collections.hasRoot(collection)  )
      yield jsonCollection(collection)

    Ok(toJson(root_collections_list))
  }

  def getAllCollections(limit : Int, showAll: Boolean) = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list = request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, showAll, limit))
          yield jsonCollection(collection)
      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list))
  }



  def getTopLevelCollections() = PermissionAction(Permission.ViewCollection){ implicit request =>
    implicit val user = request.user
    val count = collections.countAccess(Set[Permission](Permission.ViewCollection),user,true)
    val limit = count.toInt
    val top_level_collections = for (collection <- collections.listAccess(limit,Set[Permission](Permission.ViewCollection),request.user,true, true,false); if (collections.hasRoot(collection) || collection.parent_collection_ids.isEmpty))
      yield jsonCollection(collection)
    Ok(toJson(top_level_collections))
  }

  def getChildCollectionIds(collectionId: UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection,collectionId))){implicit request =>
    collections.get(collectionId) match {
      case Some(collection) => {
        val childCollectionIds = collection.child_collection_ids
        Ok(toJson(childCollectionIds))
      }
      case None => BadRequest(toJson("collection not found"))
    }
  }

  def getParentCollectionIds(collectionId: UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection,collectionId))){implicit request =>
    collections.get(collectionId) match {
      case Some(collection) => {
        val parentCollectionIds = collection.parent_collection_ids
        Ok(toJson(parentCollectionIds))
      }
      case None => BadRequest(toJson("collection not found"))
    }
  }



  def getChildCollections(collectionId: UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection,collectionId))){implicit request =>
    collections.get(collectionId) match {
      case Some(collection) => {
        val childCollections = ListBuffer.empty[JsValue]
        val childCollectionIds = collection.child_collection_ids
        for (childCollectionId <- childCollectionIds) {
          collections.get(childCollectionId) match {
            case Some(child_collection) => {
              childCollections += jsonCollection(child_collection )
            }
            case None =>
              Logger.debug("No child collection with id : " + childCollectionId)
              collections.removeSubCollectionId(childCollectionId,collection)
          }
        }

        Ok(toJson(childCollections))
      }
      case None => BadRequest(toJson("collection not found"))
    }
  }

  def getParentCollections(collectionId: UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection,collectionId))){implicit request =>
    collections.get(collectionId) match {
      case Some(collection) => {
        val parentCollections = ListBuffer.empty[JsValue]
        val parentCollectionIds = collection.parent_collection_ids
        for (parentCollectionId <- parentCollectionIds) {
          collections.get(parentCollectionId) match {
            case Some(parent_collection) => {
              parentCollections += jsonCollection(parent_collection )
            }
            case None =>
              Logger.debug("No parent collection with id : " + parentCollectionId)
              collections.removeParentCollectionId(parentCollectionId,collection)
          }
        }

        Ok(toJson(parentCollections))
      }

      case None => BadRequest(toJson("collection not found"))
    }
  }

  def removeFromSpaceAllowed(collectionId: UUID , spaceId : UUID) = PermissionAction(Permission.AddResourceToSpace, Some(ResourceRef(ResourceRef.space, spaceId))) { implicit request =>
    val user = request.user
    user match {
      case Some(identity) => {
        val hasParentInSpace = collections.hasParentInSpace(collectionId, spaceId)
        Ok(toJson(!(hasParentInSpace)))
      }
      case None => Ok(toJson(false))
    }
  }

  def download(id: UUID, compression: Int) = PermissionAction(Permission.DownloadFiles, Some(ResourceRef(ResourceRef.collection, id))) { implicit request =>
    implicit val user = request.user
    collections.get(id) match {
      case Some(collection) => {
        val bagit = play.api.Play.configuration.getBoolean("downloadCollectionBagit").getOrElse(true)
        // Use custom enumerator to create the zip file on the fly
        // Use a 1MB in memory byte array
        Ok.chunked(enumeratorFromCollection(collection,1024*1024, compression,bagit,user)).withHeaders(
          "Content-Type" -> "application/zip",
          "Content-Disposition" -> ("attachment; filename=\"" + collection.name+ ".zip\"")
        )
      }
      // If the dataset wasn't found by ID
      case None => {
        NotFound
      }
    }
  }


  def enumeratorFromCollection(collection: Collection, chunkSize: Int = 1024 * 8, compression: Int = Deflater.DEFAULT_COMPRESSION, bagit: Boolean, user : Option[User])
                              (implicit ec: ExecutionContext): Enumerator[Array[Byte]] = {

    implicit val pec = ec.prepare()
    val md5Files = scala.collection.mutable.HashMap.empty[String, MessageDigest]
    val md5Bag = scala.collection.mutable.HashMap.empty[String, MessageDigest]

    var totalBytes = 0L

    val byteArrayOutputStream = new ByteArrayOutputStream(chunkSize)
    val zip = new ZipOutputStream(byteArrayOutputStream)

    var bytesSet = false

    //val datasetsInCollection = getDatasetsInCollection(collection,user.get)
    var current_iterator = new RootCollectionIterator(collection.name,collection,zip,md5Files,md5Bag,user, totalBytes,bagit,collections,
    datasets,files,folders,metadataService,spaces)



    //var current_iterator = new FileIterator(folderNameMap(inputFiles(1).id),inputFiles(1), zip,md5Files)
    var is = current_iterator.next()

    Enumerator.generateM({
      is match {
        case Some(inputStream) => {
          if (current_iterator.isBagIt() && bytesSet == false){
            current_iterator.setBytes(totalBytes)
            bytesSet = true
          }
          val buffer = new Array[Byte](chunkSize)
          val bytesRead = scala.concurrent.blocking {
            inputStream.read(buffer)

          }
          val chunk = bytesRead match {
            case -1 => {
              zip.closeEntry()
              inputStream.close()
              Some(byteArrayOutputStream.toByteArray)
              if (current_iterator.hasNext()){
                is = current_iterator.next()
              } else{
                zip.close()
                is = None
              }
              Some(byteArrayOutputStream.toByteArray)
            }
            case read => {
              if (!current_iterator.isBagIt()){
                totalBytes += bytesRead
              }
              zip.write(buffer, 0, read)
              Some(byteArrayOutputStream.toByteArray)
            }
          }
          byteArrayOutputStream.reset()
          Future.successful(chunk)
        }
        case None => {
          Future.successful(None)
        }
      }
    })(pec)

  }

  def changeOwnerCollection(collectionId : UUID, newOwner : User, oldOwner : User)  {
    collections.get(collectionId) match {
      case Some(col) => {
        if (col.author.id == oldOwner.id){

          val datasetsInCol = datasets.listCollection(collectionId.stringify,Some(oldOwner))
          collections.changeOwner(oldOwner.id, newOwner, collectionId)
          val child_collections : List[UUID] = col.child_collection_ids

          for (dataset <- datasetsInCol){
            changeOwnerDataset(dataset.id,newOwner,oldOwner.id)
          }

          for (child_id <- child_collections){
            changeOwnerCollection(child_id,newOwner,oldOwner)
          }

        } else {
          Logger.info("Could not change owner of collection : " + collectionId + " not same previous owner")
        }
      }
      case None => Logger.error("Could not change owner of dataset : " +  collectionId + " not found")
    }
  }

  def changeOwnerCollectionOnly(collectionId : UUID, newOwnerId : UUID) = PrivateServerAction {implicit request =>
    val newOwner = userService.get(newOwnerId)
    collections.get(collectionId) match {
      case Some(col) => {
        val oldOwner = col.author.id
        collections.changeOwner(oldOwner, newOwner.get, collectionId)
        collections.get(collectionId) match {
          case Some(fixed_col) => {
            Ok(toJson(Map("newAuthor"->fixed_col.author.id.toString)))
          }
          case None => BadRequest("Change author - error")
        }
      }
      case None => BadRequest("Failed to change owner for collection " + collectionId)
    }

  }

  def changeOwnerDatasetOnly(datasetId : UUID, newOwnerId : UUID) = PrivateServerAction {implicit request =>
    val newOwner = userService.get(newOwnerId)
    datasets.get(datasetId) match {
      case Some(d) => {
        val oldOwnerId = d.author.id
        datasets.changeOwner(oldOwnerId, newOwner.get, datasetId)
        Ok(toJson(Map("status"->"success")))
      }
      case None => BadRequest("Failed to change owner for dataset id " + datasetId)
    }

  }

  def changeOwnerFileOnly(fileId : UUID, newOwnerId : UUID) = PrivateServerAction {implicit request =>
    val newOwner = userService.get(newOwnerId)
    files.get(fileId) match {
      case Some(f) => {
        val oldOwnerId = f.author.id
        files.changeOwner(oldOwnerId,newOwner.get, f.id)
        files.get(f.id) match {
          case Some(changedFile) => {
            val currentAuthor = changedFile.author
            val currentAuthorId = changedFile.author.id
            if (currentAuthorId == newOwnerId){
              Ok(toJson(Map("status"->"success")))
            }
            else {
              Ok(toJson(Map("status"->"failure")))
            }
          }
          case None => BadRequest("Something went wrong")
        }

      }
      case None => BadRequest("Failed to change owner for file id " + fileId)
    }
  }

  def changeOwnerDataset(datasetId : UUID, newOwner : User, oldOwnerId : UUID) {
    datasets.get(datasetId) match {
      case Some(ds) => {
        if (ds.author.id == oldOwnerId){
          datasets.changeOwner(oldOwnerId, newOwner, datasetId)
          val filesOfDataset : List[UUID] = ds.files
          for (f <- filesOfDataset){
            changeOwnerFile(f,newOwner, oldOwnerId)
          }
        } else {
          Logger.info("Could not change owner of dataset : " + datasetId + " not same previous owner")
        }
      }
      case None => Logger.error("Could not change owner of dataset : " +  datasetId + " not found")
    }
  }

  private def hasOtherParent(parentCollection : Collection, childCollection : Collection) : Boolean = {
   val parents_of_child = childCollection.parent_collection_ids
   var foundOtherParent = false
    for (parent_id <- parents_of_child){
      if (parent_id != parentCollection.id){
        foundOtherParent = true
        return true
      }
    }
    return foundOtherParent
  }

  private def hasOtherCollection(parentCollection : Collection, dataset : Dataset) : Boolean = {
    val dataset_collections = dataset.collections
    var otherCollection = false
    for (col <- dataset_collections){
      if (col != parentCollection.id){
        otherCollection = true
        return true
      }
    }
    return otherCollection
  }

  def deleteCollectionAndContents(coll_id : UUID, userid : UUID) = PrivateServerAction { implicit request =>
    userService.get(userid) match {
      case Some(user) => {
        collections.get(coll_id) match {
          case Some(collection) => {
            deleteCollectionAndAllContents(collection, user)
            Ok(toJson(Map("status"->"success")))

          }
          case None => BadRequest("No collection")
        }

      }
      case None => BadRequest("No user supplied")
    }
  }

  def deleteCollectionAndAllContents(collection : Collection, user : User) : Unit = {
    val child_ids = collection.child_collection_ids
    for (child_id <- child_ids) {
      collections.get(child_id) match {
        case Some(child) => {
          if (!hasOtherParent(collection, child)) {
            deleteCollectionAndAllContents(child, user)
          }
        }
        case None =>
      }
    }
    collections.update(collection.copy(trash = true, dateMovedToTrash = Some(new Date())))
    events.addObjectEvent(Some(user), collection.id, collection.name, "move_collection_trash")
    val datasetsInside = datasets.listCollection(collection.id.stringify, Some(user))
    for (dataset <- datasetsInside){
      if ((dataset.trash == false) && (dataset.author.id == user.id) ){
        if (!hasOtherCollection(collection,dataset)) {
          datasets.update(dataset.copy(dateMovedToTrash = Some(new Date()), trash= true))
          events.addObjectEvent(Some(user), dataset.id, dataset.name, "move_dataset_trash")
        }
      }
    }
  }


  def changeOwner(collectionId : UUID , userId : UUID) = PrivateServerAction (parse.json){ implicit request =>
    val user = request.user
    val newUserId = (request.body \ "newUserId").asOpt[String].getOrElse("")
    val rootId = (request.body \ "rootId").asOpt[String].getOrElse("")
    val methodKey = (request.body \"methodKey").asOpt[String].getOrElse("")
    user match {
      case Some(identity) => {
        collections.get(collectionId) match {
          case Some(col) => {
            val author : MiniUser = col.author
            val authorId = author.id
            val authorName = author.fullName
            val oldAuthor : Option[User] = userService.get(authorId)
            if ((authorId.toString() == "000000000000000000000000") || (authorName == "Anonymous User")){
              //get secret key from body
              //if secret key is the same as local secret key
              //swap user
              if (methodKey == play.Play.application().configuration().getString("4ceedKey")){
                val newUser : Option[User] = userService.get(UUID(newUserId))
                newUser match {
                  case Some(clowderUser) => {
                    changeOwnerCollection(UUID(rootId), clowderUser,oldAuthor.get)
                    Ok(toJson(Map("id"->clowderUser.id)))
                  }
                  case None => BadRequest("Error")
                }
              } else {
               BadRequest("key sent by method does not match")
              }


            } else {
              BadRequest("could not change author")
            }
          }
          case None => BadRequest("there is a user, no collection exists")
        }
      }
      case None => BadRequest("No user and no collection")
    }
  }


  def changeOwnerFile(fileId : UUID, newOwner : User, oldOwnerId : UUID) {
    files.get(fileId) match {
      case Some(f) => {
        if (f.author.id == oldOwnerId){
          files.changeOwner(oldOwnerId, newOwner, fileId)
          files.get(fileId) match {
            case Some(f) => {
              val newAuthor = f.author
              val newAuthorId = f.author.id
            }
          }
        } else {
          Logger.info("Could not change owner of dataset : " + fileId + " not same previous owner")
        }
      }
      case None => Logger.error("Could not change owner of file : "+ fileId +" not found")
    }
  }

  def detachCollectionFromParents(collection : Collection) {
    val parent_ids = collection.parent_collection_ids
    for (parent_id <- parent_ids){
      collections.removeSubCollection(parent_id,collection.id)
    }
  }

  def detachCollectionFromChildren(collection : Collection) {
    val child_ids = collection.child_collection_ids
    for (child_id <- child_ids){
      collections.removeSubCollection(collection.id,child_id)
    }
  }

  def detachCollectionFromDatasets(collection : Collection, user : User) {
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, Some(user))
    for (ds <- datasetsInCollection){
      datasets.removeCollection(ds.id,collection.id)
    }
  }

  def detachCollection(collection : Collection, user : User) {
    detachCollectionFromParents(collection)
    detachCollectionFromChildren(collection)
    detachCollectionFromDatasets(collection,user)
  }

}

