package models

import java.sql.Time
import java.util.Date

/**
  * Created by telgama2 on 10/3/17.
  */
/**
  * Jupyter Notebook
  *
  *
  */
case class Notebook(
   id: UUID = UUID.generate,
   name: String,
   userID: UUID,
   htmlfile: String="",
   notebookfile: String="",
   notebookfileContent: String="",
   htmlfileContent: String=""
   //containerId : Container
 )


