package models
import java.awt.TextArea
import java.sql.Time
import java.util.Date

/**
  * Created by telgama2 on 10/4/17.
  */
case class Container (
  id: UUID = UUID.generate,
  name: String="",
  container_file: String="",
  installed_packages: List[UUID] = List.empty
)
