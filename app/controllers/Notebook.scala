package controllers

import java.io.{FileInputStream, FileOutputStream, IOException, InputStream}
import java.nio.file.Files
import java.util.zip.{ZipEntry, ZipInputStream}
import javax.activation.MimetypesFileTypeMap
import play.api.Logger

import scala.sys.process._
import api.Permission._
import fileutils.FilesUtils
import models._
import org.apache.commons.lang.StringEscapeUtils._
import util.{FileUtils, Formatters, RequiredFieldsConfig}
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.{Inject, Singleton}

import api.{Permission, routes}
import services.{CollectionService, DatasetService, _}

import scala.collection.immutable.List
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import services._
import services.mongodb.{MongoDBAnalysisSessionService, MongoDBContainerService, MongoDBNotebookService}
import org.apache.commons.lang.StringEscapeUtils


/**
  * Jupyter Notebook Integration
  */
@Singleton
class Notebook @Inject()(containers: MongoDBContainerService, sessions: MongoDBAnalysisSessionService, notebooks: MongoDBNotebookService) extends SecuredController {


  /**
    * Utils
    */
  def extensor(orig: String, ext: String) = (orig.split('.') match {
    case xs@Array(x) => xs
    case y => y.init
  }) :+ ext mkString "."


  /**
    * List Notebooks
    */
  def listNotebooks() = PrivateServerAction { implicit request =>
    implicit val user = request.user
    user match {
      case Some(clowderUser) => {
        Logger.debug("Listing ntebooks")
        var notebookList = notebooks.list(clowderUser.id);
        Ok(views.html.listnotebook(notebookList))
      }
      case None => InternalServerError("No user defined")
    }
  }

  /*
   * View Notebook
   */
  def viewNotebook(id: UUID) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    Logger.debug("Viewing Notebook")
    var notebookToView = notebooks.get(id)
    notebookToView match {
      case Some(notebook) => {
        val htmlNotebookName = extensor(notebook.name, "html")
        //Ok(views.html.viewnotebook(htmlNotebook))
        val htmlNotebookContent = notebook.htmlfileContent
        Ok(views.html.viewnotebook(htmlNotebookName,htmlNotebookContent))
      }
      case None => InternalServerError("No notebook with id " + id.toString())
    }

  }

  /*
  * Edit Notebook
  */
  def editNotebook(id: UUID) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    user match {
      case Some(clowderUser) => {
        Logger.debug("Editing Notebook")
        var notebookList = notebooks.list(clowderUser.id);
        Ok(views.html.listnotebook(notebookList))
      }
      case None => InternalServerError("No user defined")
    }
  }

  /*
  * Delete notebook
  */
  def deleteNotebook(id: UUID) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    user match {
      case Some(clowderUser) => {
        Logger.debug("Deleting Notebook")
        notebooks.remove(id)
        var notebookList = notebooks.list(clowderUser.id);
        Ok(views.html.listnotebook(notebookList))
      }
      case None => InternalServerError("No user defined")
    }
  }



}