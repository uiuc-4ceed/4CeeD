package services.mongodb

import java.io._
import java.text.SimpleDateFormat
import java.util.{ArrayList, Date}
import javax.inject.{Inject, Singleton}

import Transformation.LidoToCidocConvertion
import util.{Parsers, Formatters}
import api.Permission
import api.Permission.Permission
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.WriteConcern
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.util.JSON
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import jsonutils.JsonUtil
import models.{File, _}
import org.apache.commons.io.FileUtils
import org.bson.types.ObjectId
import org.json.JSONObject
import play.api.Logger
import play.api.Play._
import play.api.libs.json.Json._
import play.api.libs.json.{Json, JsValue, JsArray}
import services._
import services.mongodb.MongoContext.context

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}
import scala.util.parsing.json.JSONArray
/**
  * Created by telgama2 on 10/10/17.
  */
@Singleton
class MongoDBContainerService  @Inject()() extends ContainerService {
  /**
    * Get container.
    */
  def get(id: UUID): Option[Container] = {
    Container.findOneById(new ObjectId(id.stringify))
  }

  /**
    * Create container
    */
  def insert(container: Container): Option[String] = {
    Container.insert(container).map(_.toString)
  }
}

object Container extends ModelCompanion[Container, ObjectId] {
  val dao = current.plugin[MongoSalatPlugin] match {
    case None => throw new RuntimeException("No MongoSalatPlugin");
    case Some(x) => new SalatDAO[Container, ObjectId](collection = x.collection("containers")) {}
  }
}

