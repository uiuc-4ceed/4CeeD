package services.mongodb


import api.Permission
import api.Permission.Permission
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.WriteConcern
import models._
import com.mongodb.casbah.commons.MongoDBObject
import java.text.SimpleDateFormat
import org.bson.types.ObjectId
import play.api.Logger
import util.Formatters
import scala.collection.mutable.ListBuffer
import scala.util.Try
import services._
import javax.inject.{Singleton, Inject}
import scala.util.Failure
import scala.util.Success
import MongoContext.context
import play.api.Play._
/**
  * Created by telgama2 on 12/14/17.
  */

@Singleton
class MongoDBNotebookService @Inject() extends NotebookService {
  /**
    * Get Notebok.
    */
  def get(id: UUID): Option[Notebook] = {
    Notebook.findOneById(new ObjectId(id.stringify))
  }

  def remove(id: UUID) {
    //Notebook.remove(new ObjectId(id.stringify))
    Notebook.remove(MongoDBObject("_id" -> new ObjectId(id.stringify)))
  }

  def insert(notebook: Notebook): Option[String] = {
    Notebook.insert(notebook).map(_.toString)
  }
  /**
    * Return a list of the requested Notebooks
    */
  def list(): List[Notebook] = {
    (for (notebook <- Notebook.find(MongoDBObject())) yield notebook).toList
  }

  /**
    * Return a list of the requested Notebooks for a specific user
    */
  def list(userID : UUID): List[Notebook] = {
    (for (notebook <- Notebook.find(MongoDBObject("userID" -> new ObjectId(userID.stringify)))) yield notebook).toList
  }
}

object Notebook extends ModelCompanion[Notebook, ObjectId] {
  val dao = current.plugin[MongoSalatPlugin] match {
    case None => throw new RuntimeException("No MongoSalatPlugin");
    case Some(x) => new SalatDAO[Notebook, ObjectId](collection = x.collection("notebooks")) {}
  }
}

