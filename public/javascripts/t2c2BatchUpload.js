//Populate colection and dataset menus on load
$(document).ready(function() {
	$("#fileSubmit").hide(); 
	$("#formGetSpaces").hide(); 
	$("#spaceShowSelectedUsers").hide(); 
	$("#spaceShowEmailUsers").hide(); 	
	$("#emailLabel").hide(); 	
	$("#colLink").hide(); 

	getUserId(); 
});

//handle whether space options should be shown. 
$(document).ready(function () {

  $('input[name=showSharedPanel]:radio').click(function () {   
    radioValue($('input[name=showSharedPanel]:radio:checked'));
  });

  radioValue($('input[name=showSharedPanel]:radio:checked'));
});

function sortJsonDatasetName(a,b){
		return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
};

function radioValue(jqRadioButton){
  if (jqRadioButton.length) {

    showSharedPanel = jqRadioButton.val();
    if (showSharedPanel === 'yes'){
    	$("#formGetSpaces").show(); 
	    $("#fileSubmit").hide(); 
    	getSpaces(); 

    }else{
    	$("#formGetSpaces").hide(); 
	    $("#fileSubmit").show(); 
    }
  }
  else {
    showSharedPanel = 0;
  }
}

$(document).on('click', 'a[href="#space2"]', function(){
	getUserList(); 
}); 

$(document).on('click', 'a[href="#space1"]', function(){
	if ($("#spaces").val() === ""){
		$("#fileSubmit").hide("slow"); 
	}else{
		$("#fileSubmit").show("slow"); 
	}
}); 

$(document).on('change', '#spaces', function(){
	if ($("#spaces").val() !== ""){
		$("#fileSubmit").show("slow"); 
		var spaceId = $("#spaces").val(); 
		var newUrl = jsRoutes.api.Collections.uploadZipToSpace(spaceId).url;
		uploadObj.update({url:newUrl});	 
	}else{
		$("#fileSubmit").hide("slow"); 
	}
});	

function getUserList(responseData){

	$("#spaceActiveUsers").autocomplete({

		source:function(request, response){
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );			
		    $.ajax({
				url: jsRoutes.api.Users.list().url,
		        type: "GET",
		        dataType: "json",
				beforeSend: function(xhr){
					xhr.setRequestHeader("Content-Type", "application/json"); 
					xhr.setRequestHeader("Accept", "application/json");
				}, 		     

				success: function (data) {
					//remove active user from the list
	                response($.map(data, function(v,i){
	                    var text = v.fullName;
	                    if (v.id != ($("#userId").val())){
		                    if ( text && ( !request.term || matcher.test(text) ) ) {
		                        return {
		                                value: v.fullName,
		                                id: v.id
		                        };
		                    }
	                	}
	                }));
	            }				   
		    });
		}, 
		select: function (e, ui) {
			$("#spaceShowSelectedUsers").show(); 
			var div = $("<div />"); 
			div.html(createUserList(ui.item.label, ui.item.id)); 
			$(".userRoleInfo").append(div); 
			$(this).val(''); return false; 			
		}

	});
}

var counter2 = new counter();

function createUserList(name, id){

	var i = counter2.add();    
    return 	'<div class="pad_this_right row userAddedBySearch"><div class="col-3"><b> ' + name + ' </b>' + 
    '<input type="hidden" value=' + id + ' class="userNameList" id = ' + id + ' /></div>' +
    '<div class="col-6 spaceRadioGroup">'+ 
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" value="Admin">Admin</label>' +
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" checked="checked" value="Editor">Editor</label>' +
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" value="Viewer">Viewer</label>' +
	'<span class="pad_this_right"><button type="button" class="btn btn-danger removeUser">Remove</button></span></div><hr /></div>'	
}

function updateUsersSpace(spaceId, rolesList){
	$.ajax({
		url: jsRoutes.api.Spaces.updateUsers(spaceId).url,
		type:"POST", 
		dataType: "json",
		data: rolesList,
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
		}
	});

}

$(document).on('click', '.removeUser', function(){
 	$(this).closest('.userAddedBySearch').remove();

 	var len = $('.userAddedBySearch').length; 
 	if (len  > 0){
 		$("#spaceShowSelectedUsers").show(); 
 	}else{
 		$("#spaceShowSelectedUsers").hide(); 
 	}
}); 

$(document).on('click', '.email-btn', function(){
 	$(this).closest('.infoToEmail').remove();

 	var len = $('.infoToEmail').length; 
 	if (len  > 0){
 		$("#spaceShowEmailUsers").show(); 
 	}else{
 		$("#spaceShowEmailUsers").hide(); 
 	}
}); 


var counter3 = new counter();

var userEmailAndRole = []; 

function validateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

$(document).on('click', '.createEmailUser', function(){

	 if (validateEmail($(".spaceNonUsers").val())){
		$("#emailLabel").hide(); 	
		$("#spaceShowEmailUsers").show();

		var i = counter3.add();    
		var div = $("<div class='row infoToEmail' id='infoToEmail" + i + "' />");
		var userEmailAddress = $(".spaceNonUsers").val(); 
		var userEmailRole = $(".spaceEmailRole").val(); 
		$(".userEmailInfo").append(div);
		$("#infoToEmail" + i + "").append("<div class='col-sm-3 userEmailAddress' id='usersEmailAddress" + i + "'>" + userEmailAddress + "</div><div class='col-sm-2 userRole' id='userRole" + i + "'>" + userEmailRole + "</div><div class='col-sm-6'><input style='width:80px; border-radius:5px;' class='btn btn-success btn-sm email-btn' value='Remove' type='button'></div></div><p></div><div class='row'><div class='col-sm-12'><hr /></div></div>");

		var myUserObj = {}; 

		myUserObj['email'] = userEmailAddress;
		myUserObj['role'] = userEmailRole; 
		userEmailAndRole.push(myUserObj);

		$(".spaceNonUsers").val("");	
		$(".spaceEmailRole").val("Editor");
	}else{
		 $("#emailLabel").show(); 	
	}
}); 

$(document).on('click', '#spaceActiveUsers', function(){
	$("#spaceActiveUsers").val(''); 
}); 

function getSpaces(newSpaceId, collectionId){
	$('#spaces').empty();
	$.ajax({	
		url: jsRoutes.api.T2C2.listMySpaces().url,
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			if (data.length > 0){
				$(".showDangerSpace").hide()

			}else{
				$(".showDangerSpace").removeClass("hidden");					
				$(".showDangerSpace").show()
				$(".viewSpaces").hide(); 
			}				
			data = $(data).sort(sortJsonDatasetName);
			$('<option>').val('').text('--Select One--').appendTo('#spaces');

			$.each(data, function(key, val) {
				$("#spaces").append($("<option></option>").val(val.id).html(val.name));
			}); 

			$("#spaces").val(newSpaceId);

				// postCollectionToSpace(spaceId, v); 


		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem loading your shared spaces.",
			  type: "error",
			  timer: 2500,
			  showConfirmButton: false
			});
		}	
	});
}

function getUserId(){
	$.ajax({	

		url: jsRoutes.api.Users.getUser().url,
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			$("#userId").val(data.id);
		}

	});
}

var userId; 

function updateCollectionDescription(id, comments){
	$.ajax({

		url: jsRoutes.api.Collections.updateCollectionDescription(id).url,
		type:"PUT", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ description: comments}),
		success: function(data){

		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem adding your comments to this upload",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}
	
	})

}

// load fileupload plugin
$(function(){
		var settings = {

			url: jsRoutes.api.Collections.uploadZipToSpace().url,
		    autoSubmit:true,
		    showPreview:false,
		    previewHeight: "100px",
		    previewWidth: "100px",    
		    dragDrop:true,
		    fileName: "File",
		    allowedTypes:"zip",	
		    returnType:"json",
		    showDelete:false,
		    dragdropWidth: true,
			maxFileCount: 1, 	
			showProgress: true, 
            allowDuplicates: true,
            showDownload: false,
            statusBarWidth: "auto",
            showFileSize: true,
            showDone: false, 
            showAbort: false,
            // maxFileSize: 5000000000,
            formData: true, 
			// extraHTML:function()
			// {
			//   //   	var html = "<div class='jumbotron'><b>Comments:</b><br /><textarea name='description' class='form-control description' id='description'></textarea>";
			// 		// html += "</div><br />";
			// 		// return html;    		
			// },			
			onSuccess:function(files,data,xhr,pd){
	
				// $.each(data, function(k,v){
				// 	// updateCollectionDescription(v, $("#description").val());
				// 	if ($("#spaces").val() != "" && $("#spaces").val() != null){
				// 		var spaceId = $("#spaces").val(); 
				// 		postCollectionToSpace(spaceId, v); 
				// 	}
				// 	showLinkToUpload(v, files[0]); 
				// }); 

				$("#colLink").show(); 
				// updateCollectionDescription(data.newCollectionId, $("#description").val());
			}, 

			afterUploadAll:function(obj)
			{
				swal({
				  title: "Success", 
				  text: "Your files were uploaded",
				  type: "success",
				  timer: 1500,
				  showConfirmButton: false
				});		

				$(".showSharedPanel").prop('checked', false);			    				
				counter2.reset();
				counter3.reset();
			    uploadObj.reset();
			    $("#fileSubmit").hide(); 
			    $("#formGetSpaces").hide();				
			    //You can get data of the plugin using obj
			},

			onError: function(files,status,errMsg,pd){
				swal({
				  title: "Error", 
				  text: "Your files were not uploaded",
				  type: "error",
				  timer: 1500,
				  showConfirmButton: false
				});				
			},		
			onSelect:function(files)
			{
			    return true; //to allow file submission.
			},
			beforeSend: function() 
			{
			 
			},
		    deleteCallback: function(data,pd) {
			    for(var i=0;i<data.length;i++) {
			        $.post("delete.php",{op:"delete",name:data[i]},
			        function(resp, textStatus, jqXHR) {
			            //Show Message  
			            $("#status").append("<div>File Deleted</div>");      
			        });
			     }      
			    pd.statusbar.hide(); 
			},
			downloadCallback:function(filename,pd)
			{
			},
	    	customProgressBar: function(obj,s)
	        {
	            this.statusbar = $("<div class='ajax-file-upload-statusbar'></div>");
	            this.preview = $("<img class='ajax-file-upload-preview' />").width(s.previewWidth).height(s.previewHeight).appendTo(this.statusbar).hide();
	            this.filename = $("<div class='ajax-file-upload-filename'></div>").appendTo(this.statusbar);
	            this.progressDiv = $("<div class='ajax-file-upload-progress'>").appendTo(this.statusbar).hide();
	            this.progressbar = $("<div class='ajax-file-upload-bar'></div>").appendTo(this.progressDiv);
	            this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
	            this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
	            this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
	            this.download = $("<div><a href=''>" + s.downloadStr + "</a></div>").appendTo(this.statusbar).hide();
	            this.del = $("<div>" + s.deletelStr + "</div>").appendTo(this.statusbar).hide();

	            this.abort.addClass("ajax-file-upload-red");
	            this.done.addClass("ajax-file-upload-green");
	            this.download.addClass("ajax-file-upload-green");            
	            this.cancel.addClass("ajax-file-upload-red");
	            this.del.addClass("ajax-file-upload-red");
	            // if(count++ %2 ==0)
	            //     this.statusbar.addClass("even");
	            // else
	            //     this.statusbar.addClass("odd"); 
	            // return this;
	        }			

		}

	  uploadObj = $("#mulitplefileuploader").uploadFile(settings);

});

$(document).on('click', '.createSpace', function(e){

	e.preventDefault();
    e.stopPropagation();

    $("#formGetSpaces").validate();

    if ($("#formGetSpaces").valid()){
		postSpace(); 
    }
});

//Validate the entire form and submit collection/dataset/file

$(document).on('click', '#btnSubmit', function(e){
    e.preventDefault();
    e.stopPropagation();
	$("#colLink").empty(); 


	uploadObj.startUpload();

});	

function postSpace() {

	var spaceName = $('#spaceName').val();
	var spaceDescription = $('#spaceDescription').val();
	$.ajax({
		url: jsRoutes.api.Spaces.createSpace().url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: spaceName, description: spaceDescription}),
		success: function(data){
			swal({
			  title: "Success", 
			  text: "A new shared space was created",
			  type: "success",
			  timer: 1500,
			  showConfirmButton: false
			});

			$("#spaceName").val('');
			$("#spaceDescription").val('');
			$("#spaces").empty();

			$('#space2').collapse('hide');
			$('#space1').collapse('show');
			$(".viewSpaces").show(); 


			var buildNewUrl = jsRoutes.api.Collections.uploadZipToSpace(data.id).url;
			uploadObj.update({url:buildNewUrl});	 
			getSpaces(data.id); 

			//Parse who the admins, editors, and viewers are
			//Get author of space => Make author Admin 

			var getUserNameList = $(".userAddedBySearch .userNameList"); 
			var adminList = []; 
			var editorList = []; 
			var viewerList = []; 

			$.each(getUserNameList, function(key, val) {
				var radioVal = $('input[name=optradio' + val.id + ']:checked').val();
				if (radioVal == "Admin"){
					adminList.push(val.id);
				}else if(radioVal == "Editor"){
					editorList.push(val.id);
				}else if(radioVal == "Viewer"){
					viewerList.push(val.id);
				}
			}); 

			//add auto to admin list
			var currentUser = $("#userId").val(); 
			adminList.push(currentUser);
			var adminString = '"Admin"' + ":" + '"' + adminList.join(",") + '"';
			var editorString = '"Editor"' + ":" + '"' + editorList.join(",") + '"';
			var viewerString = '"Viewer"' + ":" + '"' + viewerList.join(",") + '"';
			
			var rolesList = "{" + adminString + "," + editorString + "," + viewerString + "}}";
			var rolesandusers = '{"rolesandusers":';  
			updateUsersSpace(data.id, rolesandusers + rolesList); 

			if (userEmailAndRole.length > 0 && typeof userEmailAndRole !== 'undefined'){
				sendEmailInvite(data.id, userEmailAndRole); 
			}
			$(".userAddedBySearch").remove();
			$(".infoToEmail").remove();
			$("#spaceShowSelectedUsers").hide();
			$("#spaceShowEmailUsers").hide();
			$("#fileSubmit").show(); 

	}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating a shared space",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

function sendEmailInvite(spaceId, inviteRoleList){
	var txt = {"message": "","invites":inviteRoleList}; 
	$.ajax({	
		url: jsRoutes.api.Spaces.inviteToSpace(spaceId).url,
		type:"POST", 		
		dataType: "json",
		data: JSON.stringify(txt),
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
		}

	});
}

function postCollectionToSpace(spaceId, collectionId) {
	$.ajax({
		url: jsRoutes.api.Spaces.addCollectionToSpace(spaceId, collectionId).url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: spaceName, description: spaceDescription}),
		success: function(data){

			$("#spaces").val('');
			$("#spaces").empty();
			getSpaces(spaceId, collectionId); 

	}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating a shared space",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

var counter1 = new counter(); 
function showLinkToUpload(collectionId, fileName){
	
	var i = counter1.add();    
	var newLink = '<span class="glyphicon glyphicon-folder-close" style="padding-right:5px;"></span><a target="_blank" href="/collection/'+collectionId+'" class="linkUpload'+i+'">'+fileName+'</a><br />'; 

	$("#colLink").append(newLink); 
	$("#colLink").show(); 

}

function counter() {
   var count = 0;

   this.reset = function() {
       count = 0;
       return count;
   };

   this.add = function() {
       return ++count;
   };
}
