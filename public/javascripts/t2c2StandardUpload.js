//Populate colection and dataset menus on load
$(document).ready(function() {
	$("#fileSubmit").hide(); 
	$("#formGetDatasets").hide(); 
	$("#formGetSpaces").hide(); 
	$("#formGetCollections").hide(); 
	$("#spaceShowSelectedUsers").hide(); 
	$("#spaceShowEmailUsers").hide(); 	
	$("#emailLabel").hide(); 	
	$("#colLink").hide(); 

	getUserId(); 
    $('[data-toggle="popover"]').popover(); 
 	setDatasetID = ""; 
});

function hideDivs() {
	$("#fileSubmit").hide(); 
	$("#formGetDatasets").hide(); 
}

//handle whether space options should be shown. 
$(document).ready(function () {

  $('input[name=showSharedPanel]:radio').click(function () {   
    radioValue($('input[name=showSharedPanel]:radio:checked'));
  });

  radioValue($('input[name=showSharedPanel]:radio:checked'));
});

var userId; 
var showSharedPanel;

function radioValue(jqRadioButton){
  if (jqRadioButton.length) {
    showSharedPanel = jqRadioButton.val();
    if (showSharedPanel === 'yes'){
    	$("#formGetSpaces").show(); 
	    $("#formGetCollections").hide(); 
		$("#formGetDatasets").hide(); 
		$("#fileSubmit").hide(); 
    	getSpaces(); 
		if ($("#spaces").val() == null){
			$("#formGetCollections").hide(); 
			$("#formGetDatasets").hide(); 
		}else{
			$("#formGetCollections").hide(); 
			$("#formGetDatasets").hide(); 
			$("#fileSubmit").hide(); 
			var spaceId = $("#spaces").val(); 
			createJSTrees("getLevelOfTreeInSpace", "", spaceId, "");
		}
    }else{
    	$("#spaces").val("");
    	$("#formGetSpaces").hide(); 
	    $("#formGetCollections").show(); 
		$("#formGetDatasets").hide(); 
		$("#fileSubmit").hide(); 
		// alert("not in space");
		$("#collections").jstree("destroy");
		createJSTrees("getLevelOfTreeNotShared", "", "", "");
    }
  }
  else {
    showSharedPanel = 0;
  }
}

//Set focus of textbox depending on what accordion is being shown
$(document).on('shown.bs.collapse', '#accordion1', function(){
	$("#datasetName").focus(); 
});

$(document).on('shown.bs.collapse', '#accordion2', function(){
	$("#datasetName").focus(); 
});

$(document).on('click', '.newDS', function(e){
	$("#fileSubmit").hide(); 
});

$(document).on('click', '.existingDS', function(){
	//if there is a value for a selection then show the file panel
	if ($("#datasets").val() != "" && $("#datasets").val() != null){
		$("#fileSubmit").show(); 
	}
});

$(document).on('click', '.dsNewPanel', function(){
	$(".collapse3").hide(); 
	$(".collapse4").show(); 
	$("#fileSubmit").hide("slow"); 
});

$(document).on('click', '.dsPanel', function(){
	$(".collapse4").hide(); 
	$(".collapse3").show(); 
});

function getCurrentSelectedCollection(){
    var arr = $("#collections").jstree("get_selected");
    return arr;
}

function getCurrentSpaceSelection(){
    var spaceId = $("#spaces").val(); 

    if(spaceId.length !== 0 ) {
		var arr = $.makeArray(spaceId); 
	    return arr;
	}else{
		var arr = 0; 
		return arr; 
	}

}

$(document).on('change', '#spaces', function(){
	if ($("#spaces").val() !== ""){
		$("#collections").jstree("destroy");
		var spaceId = $("#spaces").val(); 
		createJSTrees("getLevelOfTreeInSpace", "", spaceId, "");
	    // getSharedCollections($("#spaces").val()); 
		$("#formGetCollections").show("slow"); 
	}else{
		$("#formGetCollections").hide("slow"); 
	}
});	

$(document).on('click', '#rootSpace', function(){
	$("#formGetCollections").hide("slow"); 
	$("#collections").jstree("deselect_all");	
	$("#formGetDatasets").hide("slow"); 	

}); 

$(document).on('click', 'a[href="#space2"]', function(){
	$("#formGetCollections").hide("slow"); 
	getUserList(); 
}); 

$(document).on('click', 'a[href="#space1"]', function(){
	if ($("#spaces").val() === ""){
		$("#formGetCollections").hide("slow"); 
	}else{
		$("#formGetCollections").show("slow"); 
	}
}); 

$(document).on('change', '#datasets', function(){
	setDatasetID = $("#datasets").val();
	var newUrl = "uploadToDataset/"+setDatasetID+"";
	uploadObj.update({url:newUrl});	 
	$("#fileSubmit").removeClass("hidden");					
	$("#fileSubmit").show("slow"); 
});	

$(document).on('select', '#datasets', function(){
	$("#fileSubmit").removeClass("hidden");					
	$("#fileSubmit").show("slow"); 
});	

$(document).on('click', '#rootCollection', function(){
	$("#formGetDatasets").hide("slow");
	$("#collections").jstree("deselect_all");	
}); 

$(document).on('click', '.panel-heading a', function(e){
    if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
        e.preventDefault();
        e.stopPropagation();
    }
});

// Sort dataset menus
function sortJsonDatasetName(a,b){
		return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
};

function setCurrentParentID(data, selectedText){
	var currentParentId = data; 
	postNestedCollection(currentParentId, selectedText); 
}

function getUserList(responseData){

	$("#spaceActiveUsers").autocomplete({

		source:function(request, response){
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );			
		    $.ajax({
				url: jsRoutes.api.Users.list().url,
		        type: "GET",
		        dataType: "json",
				beforeSend: function(xhr){
					xhr.setRequestHeader("Content-Type", "application/json"); 
					xhr.setRequestHeader("Accept", "application/json");
				}, 		     

				success: function (data) {
					//remove active user from the list
	                response($.map(data, function(v,i){
	                    var text = v.fullName;
	                    if (v.id != ($("#userId").val())){
		                    if ( text && ( !request.term || matcher.test(text) ) ) {
		                        return {
		                                value: v.fullName,
		                                id: v.id
		                        };
		                    }
	                	}
	                }));
	            }				   
		    });
		}, 
		select: function (e, ui) {
			$("#spaceShowSelectedUsers").show(); 
			var div = $("<div />"); 
			div.html(createUserList(ui.item.label, ui.item.id)); 
			$(".userRoleInfo").append(div); 
			$(this).val(''); return false; 			
		}

	});
}

var counter2 = new counter();

function createUserList(name, id){

	var i = counter2.add();    
    return 	'<div class="pad_this_right row userAddedBySearch"><div class="col-3"><b> ' + name + ' </b>' + 
    '<input type="hidden" value=' + id + ' class="userNameList" id = ' + id + ' /></div>' +
    '<div class="col-6 spaceRadioGroup">'+ 
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" value="Admin">Admin</label>' +
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" checked="checked" value="Editor">Editor</label>' +
    '<label class="radio-inline">' +
    '<input type="radio" name="optradio' + id + '" value="Viewer">Viewer</label>' +
	'<span class="pad_this_right"><button type="button" class="btn btn-danger removeUser">Remove</button></span></div><hr /></div>'	
}

function updateUsersSpace(spaceId, rolesList){
	$.ajax({
		url: jsRoutes.api.Spaces.updateUsers(spaceId).url,
		type:"POST", 
		dataType: "json",
		data: rolesList,
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
		}
	});

}

$(document).on('click', '.removeUser', function(){
 	$(this).closest('.userAddedBySearch').remove();

 	var len = $('.userAddedBySearch').length; 
 	if (len  > 0){
 		$("#spaceShowSelectedUsers").show(); 
 	}else{
 		$("#spaceShowSelectedUsers").hide(); 
 	}
}); 

$(document).on('click', '.email-btn', function(){
 	$(this).closest('.infoToEmail').remove();

 	var len = $('.infoToEmail').length; 
 	if (len  > 0){
 		$("#spaceShowEmailUsers").show(); 
 	}else{
 		$("#spaceShowEmailUsers").hide(); 
 	}
}); 

var counter3 = new counter();

var userEmailAndRole = []; 

function validateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

$(document).on('click', '.createEmailUser', function(){

	 if (validateEmail($(".spaceNonUsers").val())){
		$("#emailLabel").hide(); 	
		$("#spaceShowEmailUsers").show();

		var i = counter3.add();    
		var div = $("<div class='row infoToEmail' id='infoToEmail" + i + "' />");
		var userEmailAddress = $(".spaceNonUsers").val(); 
		var userEmailRole = $(".spaceEmailRole").val(); 
		$(".userEmailInfo").append(div);
		$("#infoToEmail" + i + "").append("<div class='col-sm-3 userEmailAddress' id='usersEmailAddress" + i + "'>" + userEmailAddress + "</div><div class='col-sm-2 userRole' id='userRole" + i + "'>" + userEmailRole + "</div><div class='col-sm-6'><input style='width:80px; border-radius:5px;' class='btn btn-success btn-sm email-btn' value='Remove' type='button'></div></div><p></div><div class='row'><div class='col-sm-12'><hr /></div></div>");

		var myUserObj = {}; 

		myUserObj['email'] = userEmailAddress;
		myUserObj['role'] = userEmailRole; 
		userEmailAndRole.push(myUserObj);

		$(".spaceNonUsers").val("");	
		$(".spaceEmailRole").val("Editor");
	}else{
		 $("#emailLabel").show(); 	
	}
}); 

$(document).on('click', '#spaceActiveUsers', function(){
	$("#spaceActiveUsers").val(''); 
}); 

function findYoungestChild(selectedText){
    var arr = $("#collections").jstree("get_selected");
    arr.pop(); 
	$.ajax({
		url: jsRoutes.api.T2C2.findYoungestChild(arr).url,
		type:"POST", 
		dataType: "json",
		data: JSON.stringify({ ids: arr}),
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			setCurrentParentID(data, selectedText); 
		}
	});
}

function getSpaces(newSpaceId, collectionId){
	$('#spaces').empty();
	$.ajax({	
		url: jsRoutes.api.T2C2.listMySpaces().url,
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			if (data.length > 0){
				$(".showDangerSpace").hide()
			}else{
				$(".showDangerSpace").removeClass("hidden");					
				$(".showDangerSpace").show()
				$(".viewSpaces").hide(); 
			}		

			data = $(data).sort(sortJsonDatasetName);
			$('<option>').val('').text('--Select One--').appendTo('#spaces');

			$.each(data, function(key, val) {
				$("#spaces").append($("<option></option>").val(val.id).html(val.name));
			}); 

			$("#spaces").val(newSpaceId);
		 	$("#collections").jstree("destroy");
			if ($("#spaces").val() != "" && $("#spaces").val() != null){
				var spaceId = $("#spaces").val(); 
				createJSTrees("getLevelOfTreeInSpace", collectionId, spaceId, ""); 
		    	// getSharedCollections(newSpaceId,collectionId); 
	   		}
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem loading your shared spaces.",
			  type: "error",
			  timer: 2500,
			  showConfirmButton: false
			});
		}	
	});
}

function getUserId(){
	$.ajax({	
		url: jsRoutes.api.Users.getUser().url,
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			$("#userId").val(data.id);
		}

	});
}

function updateUsersSpace(spaceId, rolesList){
	$.ajax({
		url: jsRoutes.api.Spaces.updateUsers(spaceId).url,
		type:"POST", 
		dataType: "json",
		data: rolesList,
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
		}
	});

}

function postSpace() {

	$("#emailLabel").hide(); 	

	var spaceName = $('#spaceName').val();
	var spaceDescription = $('#spaceDescription').val();
	$.ajax({
		url: jsRoutes.api.Spaces.createSpace().url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: spaceName, description: spaceDescription}),
		success: function(data){
			swal({
			  title: "Success", 
			  text: "A new shared space was created",
			  type: "success",
			  timer: 1500,
			  showConfirmButton: false
			});

			$("#spaceName").val('');
			$("#spaceDescription").val('');
			$("#spaces").empty();
			// $("#spaces").val('');

			$('#space2').collapse('hide');
			$('#space1').collapse('show');
			$(".viewSpaces").show(); 

			getSpaces(data.id); 

			$("#formGetCollections").show(); 

			//Parse who the admins, editors, and viewers are
			//Get author of space => Make author Admin 

			var getUserNameList = $(".userAddedBySearch .userNameList"); 
			var adminList = []; 
			var editorList = []; 
			var viewerList = []; 

			$.each(getUserNameList, function(key, val) {
				var radioVal = $('input[name=optradio' + val.id + ']:checked').val();
				if (radioVal == "Admin"){
					adminList.push(val.id);
				}else if(radioVal == "Editor"){
					editorList.push(val.id);
				}else if(radioVal == "Viewer"){
					viewerList.push(val.id);
				}
			}); 

			//add auto to admin list
			var currentUser = $("#userId").val(); 
			adminList.push(currentUser);
			var adminString = '"Admin"' + ":" + '"' + adminList.join(",") + '"';
			var editorString = '"Editor"' + ":" + '"' + editorList.join(",") + '"';
			var viewerString = '"Viewer"' + ":" + '"' + viewerList.join(",") + '"';
			
			var rolesList = "{" + adminString + "," + editorString + "," + viewerString + "}}";
			var rolesandusers = '{"rolesandusers":';  
			updateUsersSpace(data.id, rolesandusers + rolesList); 

			if (userEmailAndRole.length > 0 && typeof userEmailAndRole !== 'undefined'){
				sendEmailInvite(data.id, userEmailAndRole); 
			}
			$(".userAddedBySearch").remove();
			$(".infoToEmail").remove();
			$("#spaceShowSelectedUsers").hide();
			$("#spaceShowEmailUsers").hide();

	}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating a shared space",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

function sendEmailInvite(spaceId, inviteRoleList){
	var txt = {"message": "","invites":inviteRoleList}; 
	$.ajax({	
		url: jsRoutes.api.Spaces.inviteToSpace(spaceId).url,
		type:"POST", 		
		dataType: "json",
		data: JSON.stringify(txt),
		// data:JSON.stringify({message: "", invites : invites}), 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
		}

	});
}

function postCollectionToSpace(spaceId, collectionId) {
	$.ajax({
		// api/spaces/:spaceId/addCollectionToSpace/:collectionId 		
		url: jsRoutes.api.Spaces.addCollectionToSpace(spaceId, collectionId).url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: spaceName, description: spaceDescription}),
		success: function(data){
			swal({
			  title: "Success", 
			  text: "A new shared space was created",
			  type: "success",
			  timer: 1500,
			  showConfirmButton: false
			});
			$("#spaces").val('');
			$("#spaces").empty();
			getSpaces(spaceId, collectionId); 

	}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating a shared space",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

//Get shared collections
function getSharedCollectionsCount(spaceId){
		$.ajax({
			url: jsRoutes.api.T2C2.listCollectionsCanEdit(spaceId).url, 
			type:"GET", 
			dataType: "json",
			beforeSend: function(xhr){
				xhr.setRequestHeader("Content-Type", "application/json"); 
				xhr.setRequestHeader("Accept", "application/json");
			}, 
			success: function(data) {
				$("#datasets").attr("disabled", "disabled");	
				if (data.length > 0){
					$(".showDanger").hide()
					$(".showInfo").show()
					$(".showSearch").show()
					// createJSTrees(data,collectionId, spaceId);
					// $("#collections").val(collectionId);
				}else{
					// if ($("#formGetDatasets").is(":visible")){
					$(".showDanger").removeClass("hidden");					
					$(".showDanger").show()
					// }					
					$(".showInfo").hide()
					$(".showSearch").hide()
				}		
			}, 
			error: function(xhr, status, error) {
				swal({
				  title: "Error", 
				  text: "There was a problem returning shared collections.",
				  type: "error",
				  timer: 2500,
				  showConfirmButton: false
				});
			}	
		});
	}

function getCollectionsNum(){
	$.ajax({
		url: jsRoutes.api.T2C2.getAllCollectionsOfUserNotSharedInSpace().url, 
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			$("#datasets").attr("disabled", "disabled");	
			if (data.length > 0){
				$(".showDanger").hide()
				$(".showInfo").show()
				$(".showSearch").show()

			}else{
				$(".showDanger").removeClass("hidden");					
				$(".showDanger").show()
				$(".showInfo").hide()
				$(".showSearch").hide()

			}		
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem returning collections.",
			  type: "error",
			  timer: 2500,
			  showConfirmButton: false
			});
		}		
	});
}

//Create tree from js object


// createJSTrees("getLevelOfTreeNotShared", newCollectionID, "");

function createJSTrees(url, newCollectionID, spaceId, selectedId) {
	// alert("create tree");
	if ($("#spaces").val() != "" && $("#spaces").val() != null){
		getSharedCollectionsCount(spaceId); 
	}else{
		getCollectionsNum(); 
	}
	$('#collections').jstree({ 
		"core" : {
            "check_callback" : true,   
            'data' : {
                'url' : function (node) {
                    return node.id === "#" ?
                    "/t2c2/collections/"+url+"?currentType=collection&spaceId="+spaceId+"" :
                    "/t2c2/collections/"+url+"?currentId="+node.id+"&currentType=collection&spaceId="+spaceId+"";
                },
                'data' : function (node) {
                    return { 'id' : node.id };
                }
            },                	
			"multiple" : false,
			"themes" : {
			  "variant" : "large", 
			  "stripes" : true
			},
			"search": {
	          "case_insensitive": true,
              "show_only_matches" : true
	        },

		},
		"contextmenu":{         
		    "items": function($node) {
		        var tree = $("#collections").jstree(true);
		        return {
		            "Create": {
		                "separator_before": false,
		                "separator_after": false,
		                "label": "Create New Sub-Collection",
						"icon" : "glyphicon glyphicon-plus",	                
		                "action": function (obj) { 
		                    $node = tree.create_node($node);
		                    tree.edit($node);
		                }
		            },         		            
		            "Rename": {
		                "separator_before": false,
		                "separator_after": false,
		                "label": "Rename A Collection",
						// "icon" : "glyphicon glyphicon-plus",	                
		                "action": function (obj) { 
                    		tree.edit($node);
		                }
		            }		            
		        };
		    }
		},
		"checkbox": {
            "keep_selected_style": false, 
            "three_state": false

		}, 
		"plugins" : [
			"checkbox",
			"contextmenu", 
			"massload", 
			"search", 
			"sort", 
			"types", 
			"unique", 
			"wholerow", 
			"changed", 
			"conditionalselect"
		]

	});

	$("#collections").on("loaded.jstree", function(e, data){
		console.log("ncid", newCollectionID);
		console.log("sid", selectedId);
		// $("#collections").jstree(true).check_node("#"+newCollectionID+"");
		$('#collections').jstree('select_node', selectedId);
		// alert(newCollectionID);
	});

	$("#collections").on("create_node.jstree", function (e, data) {
			$('#collections').jstree(true).check_node(data.node.id);	
			var selectedText = data.node.text;  
			findYoungestChild(selectedText); 
			// alert("create node event");
	});

	$("#collections").on("rename_node.jstree", function (e, data) {
			$('#collections').jstree(true).check_node(data.node.id);	
			var selectedText = data.node.text;  
			editCollectionName(data.node.id, selectedText);		
	});

	$("#collections").on("select_node.jstree", function(e, data){
		 	
			var str = data.node.id;  
			if (typeof(data.node.id) === 'undefined'){	 
				var res = ""; 
			}else{
				var res = str.match(/J/gi);
			}
		 	//Prevents loading dataset for sub-collections that have no dataset yet
		 	if (res != "j" || res == null){
			 	getDatasets(data.node.id, ''); 
		 	}
			$("#fileSubmit").hide(); 

			//not in other code
			$("#formGetDatasets").removeClass("hidden");					
			$("#formGetDatasets").show(); 
		 	$(".validCollection").hide();

	});

	$("#collections").on("deselect_node.jstree", function(e, data){
		$("#datasets").attr("disabled", "disabled");	
		$("#datasets").empty(); 
		$("#fileSubmit").hide(); 
		$("#formGetDatasets").hide(); 
	});

	$("#collections").on("changed.jstree", function(e, data){
		$("#fileSubmit").hide("slow"); 
	});	

}    

//Get datasets
function getDatasets(collectionID, datasetID) {
	$('#datasets').empty();
	$.ajax({
		url: jsRoutes.api.Datasets.listInCollection(collectionID).url,
		type:"GET", 		
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {
			data = $(data).sort(sortJsonDatasetName);
			$.each(data, function(key, val) {
				$("#datasets").append($("<option class="+collectionID+"></option>").val(val.id).html(val.name));
			}); 

			$("#datasets").val(datasetID);
			setDatasetID = datasetID;
			// url: jsRoutes.api.Files.uploadToDatasetWithDescription().url,

			var newUrl = "uploadToDataset/"+setDatasetID+"";
			uploadObj.update({url:newUrl});	    

			var len = $('#datasets option').length;

			$("#collections").removeClass( "focusedInput" );					

			if (len > 0){
				//OPTIONAL
				$('#datasets').removeAttr('disabled');
				$('#datasets').addClass( "focusedInput" );	

			}else{

				//OPTIONAL
			   	$('#datasets').attr("disabled", "disabled");	
				$('#datasets').css({'background':''});
			}
		}, 
		error: function(xhr, status, error) {
		}	
	});

} 


//TEMPLATES
function getTemplates() {
	$.ajax({
		url: jsRoutes.api.Vocabularies.list().url,
		type:"GET", 
		dataType: "json",		
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			showTemplates(data); 
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem returning custom templates",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})

}

function getPublicTemplates() {
	$.ajax({
		url: jsRoutes.api.Vocabularies.getPublicVocabularies().url,
		type:"GET", 
		dataType: "json",		
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			showGlobalTemplates(data); 
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem returning custom templates",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})

}

function getTemplate(id){
	$.ajax({
		url: jsRoutes.api.T2C2.getVocabulary(id).url,
		type:"GET", 
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			createBoxes(data); 
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem returning global templates",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})	 
}

//Load Previous Datasets
function getPreviousDatasets(){
	var numberOfDatasetsToShows = 10; 
	$.ajax({
		url: jsRoutes.api.T2C2.getTemplateFromLastDataset().url,
		type:"GET", 
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			showPreviousDatasets(data); 
		}, 
		error: function(xhr, status, error) {
		}	
	})	 
}

function getPreviousDataset(id){
	$.ajax({
		url: jsRoutes.api.T2C2.getDatasetWithAttachedVocab(id).url,
		type:"GET", 
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			createBoxesForPreviousDataset(data); 
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem returning previous datasets",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})	 
}

//Load user templates
function showTemplates(data) {
	$.each(data, function(key, val) {
		$(".templates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
	}); 
	
	$(".templates").focus(); 	
}

//Load user templates
function showGlobalTemplates(templatesData) {
	$.each(templatesData, function(key, val) {
		$(".globalTemplates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
	}); 

	$(".globalTemplates").focus(); 	
}

function showTagTemplates(data){

	$('<option>').val('').text('--Select One--').appendTo('.tagTemplates');
	$.each(data, function(key, val) {
		$(".tagTemplates").append($("<option class='placeholder'></option>").val(val.template_id).html(val.name));
	}); 
	
	$(".tagTemplates").focus(); 	
}

function showPreviousDatasets(data) {
	$('<option>').val('').text('--Select One--').appendTo('.prevTemplates');
	$.each(data, function(key, val) {
		$(".prevTemplates").append($("<option class='placeholder'></option>").val(val.attached_dataset).html(val.name));
	}); 
	
	$(".prevTemplates").focus(); 	
}

function getAllTags(){

	$.ajax({
		url: jsRoutes.api.Vocabularies.getAllTagsOfAllVocabularies().url,
		type:"GET", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){

			//Format the object by removing whitespace, duplicates, and capitalize characters. 
			var showUserTags = data.join(","); 
			var allCapsTags = showUserTags.toUpperCase(); 
			var trimTags = $.map(allCapsTags.split(","), $.trim);
			var uniqueTags = jQuery.unique(trimTags);	
			$(".templateSearch").autocomplete({
				source: uniqueTags,
				select: function (event, ui) {
					$(".tagTemplates").empty(); 
					$(".tagData").show(); 

					 var selectedObj = ui.item;  
					 getByTagId(selectedObj.value);
				}, 
				change: function( event, ui ) {

				}				
			}); 
		}

	}); 
}

function getByTagId(tagId){
	$.ajax({
		url: jsRoutes.api.T2C2.getVocabIdNameFromTag(tagId).url,
		type:"GET", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){
			$(".tagData").show(); 
			// $(".otherOptions").hide(); 
			if (data.length){
				showTagTemplates(data);	
			}else{

			}
		}

	}); 
}

function createBoxes(data){
    $(".templateData").empty();
  	$(".btnDataset").show();
	$(".btnAdd").show();
  	$("#btnTemplate").show();
  	$(".otherOptions").show();
  	var menuName = $('.nav-tabs .active > a').attr("href");
  	//Get current tab and use name to determine what the label will say based on it's tab
	$.each(data.terms, function(key, val) {
        var div = $("<div />");
        div.html(createDiv(val.key, val.default_value, val.units, val.data_type, val.required));
        $(menuName + ' ' + ".templateData").append(div);
	}); 

	disableRequiredInput(); 

}

function createBoxesForPreviousDataset(data){

    $(".templateData").empty();
  	$(".btnDataset").show();
	$(".btnAdd").show();
  	$("#btnTemplate").show();
  	$(".prevOptions").show();
  	
	$.each(data.template.terms, function(i, val) {
		var div = $("<div />");
        div.html(createDiv(val.key, val.default_value, val.units, val.data_type, val.required));
		$("#prevMenu .templateData").append(div);
	}); 

	disableRequiredInput(); 

}

function clearTemplate(){
	$(".templateData").empty();
	$(".metaDataSettings").empty();
	$(".prevOptions").hide();
	$(".btnDataset").hide();
	$("#btnTemplate").hide();
	$(".templates").focus(); 
	$(".templates").val("");
	$(".prevTemplates").val("");
	$(".globalTemplates").val("");
	$(".templateSearch").val(""); 
	$(".tagTemplates").val(""); 
	$(".tagData").hide(); 
	counter1.reset();
	counter2.reset(); 
	counter3.reset(); 

}

//Run when the clear template button is clicked

$(document).on('click', '.clearTemplate', function(e){
    e.preventDefault();
    e.stopPropagation();
    clearTemplate();
});

//When advanced or create tabs are selected, clear and then get user templates
$(document).on('click', '.custMenu, .createMenu, .prevMenu', function(){

	$(".btnDataset").hide();
	$(".prevOptions").hide();
	$(".showTemplates").show(); 
	$(".showGlobalTemplates").show(); 

	$(".templates").empty(); 
	$(".globalTemplates").empty(); 
	$(".prevTemplates").empty(); 	
	$(".tagTemplates").empty(); 
	$("#btnTemplate").hide();
	$('<option>').val('').text('--Select One--').appendTo('.templates');
	$('<option>').val('').text('--Select One--').appendTo('.globalTemplates');
	$(".tagData").hide(); 

	$(".metaDataSettings").empty(); 	
	$(".templateData").empty(); 

	getTemplates();
	getPublicTemplates(); 
	getPreviousDatasets(); 	
});

//Run when basic tab is selected
$(document).on('click', '.clearMenu', function(e){
	$(".btnDataset").show();
	$("#btnTemplate").hide();
});

//Handle template load when new menu item is selected
$(document).on('change', '.templates', function(){
	var id = $(this).val(); 

	$(".tagTemplates").val([]);
	$(".tagData").hide(); 
	$(".globalTemplates").val([]);
	$(".templateSearch").val('');

	if (id != ''){
		getTemplate(id);
	}
});

$(document).on('change', '.globalTemplates', function(){
	var id = $(this).val(); 

	$(".templates").val([]);
	$(".tagTemplates").val([]);
	$(".tagData").hide(); 
	$(".templateSearch").val('');
	if (id != ''){
		getTemplate(id);
	}
});

$(document).on('change', '.prevTemplates', function(){
	var id = $(this).val(); 

	if (id != ''){
		getPreviousDataset(id);
	}
});

$(document).on('change', '.tagTemplates', function(){
	var id = $(this).val(); 

	$(".templates").val([]);
	$(".globalTemplates").val([]);

	if (id != ''){
		getTemplate(id);
	}
});

//Create a new template

$(document).on('click', '#btnTemplate', function(){

	datasetRequireAll(); 
	datasetRequiredFields(); 

	if ($("#formGetDatasets").valid()){
		postTemplate(true, ''); 
	}

}); 

//Posts new template
function postTemplate(templateType, datasetID){
	var menuName = $('.nav-tabs .active > a').attr("href");
	var templateTerms = buildTemplate(menuName); 
	var tagName = $('.tagName').val().toUpperCase(); 
	var shareTemplate = $('#checkShareTemplate').is(":checked");
    var datasetName = $("" + menuName + " .datasetName").val(); 
    var templateType = templateType.toString(); 
	$.ajax({
		url: jsRoutes.api.Vocabularies.createVocabularyFromJson(shareTemplate).url,
		type:"POST", 
		data: JSON.stringify({ name: datasetName, terms: templateTerms, tags: tagName, master: templateType}),
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data){


			if (templateType === "false"){

				swal({
				  title: "Success", 
				  text: "A new dataset was created",
				  type: "success",
				  timer: 1500,
				  showConfirmButton: false
				});
				addTemplateToDataset(datasetID, data.id);
			}else{
				swal({
				  title: "Success", 
				  text: "A new template was created",
				  type: "success",
				  timer: 1500,
				  showConfirmButton: false
				});

			 // clear all the inputs in the new dataset field tabs
			}
			$(".templateData").empty();
			$(".metaDataSettings").empty();
			$(".templates").empty(); 	
			$(".globalTemplates").empty(); 	
			$("#btnTemplate").hide();
			$(".datasetName").val(''); 
			$(".datasetDescription").val(''); 

			$(".tagName").val(''); 
			$('#checkShareTemplate').prop('checked', false); 
			$('<option>').val('').text('--Select One--').appendTo('.templates');
			$('<option>').val('').text('--Select One--').appendTo('.globalTemplates');

			$("#otherOptions").hide();
			getTemplates();			
			getPublicTemplates(); 
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating this template",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
}

function editCollectionName(collectionID, newName){

	$.ajax({
		url: jsRoutes.api.Collections.updateCollectionName(collectionID, newName).url,
		type:"PUT", 	
		data: JSON.stringify({ name: newName}),
		dataType: "json",
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		success: function(data) {	
			$("#collections").jstree("destroy");

			if ($("#spaces").val() != "" && $("#spaces").val() != null){
				var spaceId = $("#spaces").val(); 
				createJSTrees("getLevelOfTreeInSpace", collectionId, spaceId, "");
		    	// getSharedCollections($("#spaces").val(),collectionID); 
	   		}else{
				createJSTrees("getLevelOfTreeNotShared", "", "", "");

				// getCollections(collectionID); 
			}
		}, 
		error: function(xhr, status, error) {
		}	
	});
}

//Create NEW ROOT collection
function postRootCollection() {
	var collectionName = $('#collectionName').val();
	var collectionDescription = $('#collectionDescription').val();

	$.ajax({
		url: jsRoutes.api.Collections.createCollection(collectionName,collectionDescription).url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: collectionName, description: collectionDescription}),
		success: function(data){
			swal({
			  title: "Success", 
			  text: "A new collection was created",
			  type: "success",
			  timer: 1500,
			  showConfirmButton: false
			});
			$("#collectionName").val('');
			$("#collectionDescription").val('');
			$("#collections").empty();
 			$("#datasets").empty(); 
			$("#formGetDatasets").show(); 
			$('#collapse2').collapse('hide');
			$('#collapse1').collapse('show');

 			var newCollectionID = data.id

			//recreate tree	


			//Add Collection to space if a space is chosen above
			if ($("#spaces").val() != "" && $("#spaces").val() != null){
				// console.log("placing collection into space");
				var selectedSpaceId = $("#spaces").val(); 
				postCollectionToSpace(selectedSpaceId, newCollectionID)
			}else{
			 	$("#collections").jstree("destroy");
				// getCollections(newCollectionID); 
				createJSTrees("getLevelOfTreeNotShared", newCollectionID, "", newCollectionID);

			}

	}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating the collection",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

//Create NEW nested collection
function postNestedCollection(parentId, nestedCollectionName) {
	var nestedCollectionDescription = ""; //Add an optional label to jstree on create for a collection description

	 $.ajax({
		url: jsRoutes.api.Collections.createCollectionWithParent(parentId,nestedCollectionName).url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ name: nestedCollectionName, description: nestedCollectionDescription, parentId: parentId }),
		
		success: function(data){

			swal({
			  title: "Success", 
			  text: "A new sub collection was created",
			  type: "success",
			  timer: 1500,
			  showConfirmButton: false
			});

			//recreate tree	
 			$("#collections").jstree("destroy");
 			var newCollectionID = data.id
 			var currentSpaceId = $("#spaces").val();
			if ($("#spaces").val() != "" && $("#spaces").val() != null){
				var spaceId = $("#spaces").val(); 
				createJSTrees("getLevelOfTreeInSpace", newCollectionID, spaceId, newCollectionID);
	   		}else{
				createJSTrees("getLevelOfTreeNotShared", newCollectionID, "", newCollectionID);
			}
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating the sub collection",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}		
	})
} 

//Create NEW dataset
function postDatasets() {

	var menuName = $('.nav-tabs .active > a').attr("href");
	var datasetDescription = $("" + menuName + " .datasetDescription").val(); 
   
    var datasetName = $("" + menuName + " .datasetName").val(); 
	var currentNodeId = getCurrentSelectedCollection();    
	var spaceId = getCurrentSpaceSelection();  
	var params; 
	if (spaceId.length >= 1){
		params = { "name": datasetName, "description": datasetDescription, "collection": currentNodeId, "space": spaceId};
	}else{
		params = { "name": datasetName, "description": datasetDescription, "collection": currentNodeId};
	}
	$.ajax({

		url: jsRoutes.api.Datasets.createEmptyDataset(datasetName,datasetDescription).url,
		type:"POST", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify(params),
		// data: JSON.stringify({ name: datasetName, description: datasetDescription, collection: currentNodeId, space: spaceId}),
		success: function(data){

			if (menuName = "#basicMenu"){
				 postTemplate(false, data.id);	
				 getDatasets(currentNodeId, data.id); 
				 $('#collapse4').collapse('hide');
				 $('#collapse3').collapse('show');
				 $('#datasets').empty();
				 $('.datasetDescription').val('');
				 $(".templateData").empty();
				 $(".metaDataSettings").empty();	
				 $("#fileSubmit").removeClass("hidden");					
				 $("#fileSubmit").show("slow"); 
				 $(".prevOptions").hide(); 
				 $('.datasetName').val('');			 
				 $('.nav-tabs a:first').tab('show')
			}else{	

				 var selectedMenu; 
				 var templateLength = ($(menuName + " .templates option").length);
				 var globalTemplateLength = ($(menuName + " .globalTemplates option").length);
				 var tagTemplateLength = ($(menuName + " .tagTemplates option").length);
				 
				 if ($(templateLength > 1)){
				 	selectedMenu = " .templates";
				 }else if ($(globalTemplateLength > 1)){
				 	selectedMenu = " .globalTemplates";
				 }else if ($(tagTemplateLength > 1)){
				 	selectedMenu = " .tagTemplates";
				 }else{
				 	selectedMenu = " .templates";
				 }
				 addTemplateToDataset(data.id, $("" + selectedMenu + "").val());
				 getDatasets(currentNodeId, data.id); 

				 $('#collapse4').collapse('hide');
				 $('#collapse3').collapse('show');
				 $('#datasets').empty();
				 $('.datasetDescription').val('');
				 $(".templateData").empty();
				 $(".metaDataSettings").empty();	
				 $("#fileSubmit").removeClass("hidden");					
				 $("#fileSubmit").show("slow"); 
				 $(".prevOptions").hide(); 
				 $('.datasetName').val('');			 
				 $('.nav-tabs a:first').tab('show')
			}
		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating the dataset",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}	
	})
} 

function addTemplateToDataset(datasetid, templateID){
	var url = "templates/" + templateID + "/attachToDataset/" + datasetid + ""; 
	$.ajax({
		url: url, 
		// url: jsRoutes.T2C2.attachVocabToDataset(templateID,datasetid).url,
		type:"PUT", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ template_id: templateID, dataset_id: datasetid}),
		success: function(data){

		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem creating this dataset",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}
	
	})

}

function addFileDescription(id, comments){
	$.ajax({
		url: jsRoutes.api.Files.updateDescription(id).url,
		type:"PUT", 
		beforeSend: function(xhr){
			xhr.setRequestHeader("Content-Type", "application/json"); 
			xhr.setRequestHeader("Accept", "application/json");
		}, 
		data: JSON.stringify({ description: comments}),
		success: function(data){

		}, 
		error: function(xhr, status, error) {
			swal({
			  title: "Error", 
			  text: "There was a problem adding the comments to the file",
			  type: "error",
			  timer: 1500,
			  showConfirmButton: false
			});
		}
	
	})

}

// load fileupload plugin
$(function(){
		var settings = {
			url: jsRoutes.api.Files.uploadToDatasetWithDescription().url,
		    autoSubmit:false,
		    showPreview:false,
		    previewHeight: "100px",
		    previewWidth: "100px",    
		    dragDrop:true,
		    fileName: "File",
		    allowedTypes:"*",	
		    returnType:"json",
		    showDelete:false,
		    dragdropWidth: "650px",
			maxFileCount: 150, 	
			showProgress: true, 
            allowDuplicates: true,
            maxFileSize: 5000000000,
			extraHTML:function()
			{
			    	var html = "<div><b>File Comments:</b><br /><textarea class='form-control fileComments'></textarea> <br/>";
					// html += "<b>Success?</b>:<br /><select name='experimentSuccess' class='form-control'><option value=''>--Select One</option><option value='success'>Success</option><option value='failure'>Failure</option></select>";
					html += "</div><br />";
					return html;    		
			},			
			onSuccess:function(files,data,xhr,pd){
				var fileId;
				$.each(data, function(key,value){
					fileId = value[0]['id'];
				}); 

				$('#collections').jstree("deselect_all");
				$.each(fileDict, function(key, value){
					if (key == files){
						addFileDescription(fileId, value);
						hideDivs();
						$('#collections').jstree("deselect_all");
					}
				});

			}, 


			afterUploadAll:function(obj)
			{

				swal({
				  title: "Success", 
				  text: "Your files were uploaded",
				  type: "success",
				  timer: 1500,
				  showConfirmButton: false
				});		
				hideDivs();

				counter--; 

			    uploadObj.reset();
			    $("#datasets").empty(); 
			    $("#formGetCollections").hide(); 		
				$(".showSharedPanel").prop('checked', false);			    				
			    $(".spaceInfo").show(); 
			    $("#datasets").empty(); 				
			    $("#formGetSpaces").hide(); 
			    //You can get data of the plugin using obj
			},

			onError: function(files,status,errMsg,pd){
				swal({
				  title: "Error", 
				  text: "Your files were not uploaded",
				  type: "error",
				  timer: 1500,
				  showConfirmButton: false
				});				
			},		
			onSelect:function(files)
			{
			    return true; //to allow file submission.
			},
			beforeSend: function() 
			{
			 
			},
		    deleteCallback: function(data,pd) {
			    for(var i=0;i<data.length;i++) {
			        $.post("delete.php",{op:"delete",name:data[i]},
			        function(resp, textStatus, jqXHR) {
			            //Show Message  
			            $("#status").append("<div>File Deleted</div>");      
			        });
			     }      
			    pd.statusbar.hide(); 
			}
		}

	  uploadObj = $("#mulitplefileuploader").uploadFile(settings);

});

//Validate the entire form and submit collection/dataset/file

$(document).on('click', '#btnSubmit', function(){
	$("#datasets").removeAttr('disabled');
	$(".datasetName").addClass("required");

	$("#formGetDatasets").valid();

	//work around for jstree validation 
	var numSelections = $("#collections").jstree("get_selected");

	if (numSelections.length == 0){
		$("#formGetCollections").valid();
		$(".validCollection").show();
	}

	//if we have files then turn off file requirement validation
	numFiles = $('.ajax-file-upload-statusbar').length;	 
	if (numFiles == 0){
		$("#formUpload").valid();
	}

	if ($("#formGetDatasets").valid()){

		//was a dataset selected or created? 
		//This has to be done first before a fileload, since we need the datasetid
		if (setDatasetID != '' && setDatasetID != null){
			
			$(".alert").hide(); 
			comments = $(".fileComments");
            filenames = $(".ajax-file-upload-filename"); 
            fileDict = {}; 
           
            for (i = 0; i < numFiles; i++){
            	var key = $(filenames[i]).text();
            	var val = $(comments[i]).val();
            	fileDict[key.trimLeft()] = val; 
            }

			counter = numFiles - 1;
	    	uploadObj.startUpload();


	    	var getSelectedCollection = $("#collections").val(); 
			showLinkToUpload(numSelections); 

		}else{
			$("#lblDatasetCheck").text("You must select or create a dataset before uploading a file.")
		}
	 }

});	

function showLinkToUpload(collectionId){

	$("#linkToCollection").attr("href", "/collection/"+collectionId+"");
	$("#colLink").show(); 

}
//Removes previous error messages when a new tab is selected

$(document).on('click', '.custMenu, .createMenu, .clearMenu, .prevMenu', function(){

	var validator = $("#formGetDatasets").validate();
	validator.resetForm();
	$("label.error").hide();
    $(".error").removeClass("error");	
	counter1.reset();
	counter2.reset();
	counter3.reset();

}); 

//Used to disable inputs in loaded template that are required
function disableRequiredInput(){

	 var menuName = $('.nav-tabs .active > a').attr("href");
	 if (menuName != "#createMenu"){

		$.each($('.requireField'), function(idx){

			var currentId = (this.id);
			var counter = currentId.match(/\d+/); 

			if ($(this).val() == "true") {
				$("#metaDataKey" + counter).attr("disabled", true); 
				$("#metaDataUnit" + counter).attr("disabled", true); 
				$("#metaDataType" + counter).attr("disabled", true); 
				$("#btnRemove" + counter).attr("disabled", true);
				$("#requireField" + counter).attr("disabled", true);

			}

			$("#requireField" + counter).attr("disabled", true);

			// $("#metaDataKey" + counter).attr("disabled", true);
			// $("#metaDataUnit" + counter).attr("disabled", true);
			// $("#metaDataType" + counter).attr("disabled", true);

		}); 
	 }
}

$("#formGetDatasets").validate();

$(document).on('click', '.btnDataset', function(e){

  	var menuName = $('.nav-tabs .active > a').attr("href");
	var jsTreeValid = false; 

	// work around for jstree validation 
	var numSelections = $("#collections").jstree("get_selected");

	if (numSelections.length == 0){
		$(".validCollection").show();
		jstreeValid = false; 
	}else{
		jstreeValid = true; 
	}

	datasetRequireAll(); 
	datasetRequiredFields(); 

	if ($("#formGetDatasets").valid() && jstreeValid == true){
		postDatasets(e); 

	}
});	

//If row is required
function datasetRequireAll(){

	$.each($('.requireField'), function(idx){

		var menuName = $('.nav-tabs .active > a').attr("href");

		var currentId = (this.id);
		var counter = currentId.match(/\d+/); 
		if ($(this).val() == "true") {
			$("#metaDataKey" + counter).rules('add', {
				required: true, 
			    maxlength: 50
			}); 
			// $("#metaDataUnit" + counter).rules('add', {
			// 	required: true, 
			//     maxlength: 50
			// }); 

			$("#metaDataType" + counter).rules('add', {
				required: true, 
			    maxlength: 50
			}); 
	 		
	 		if (menuName != "#createMenu"){

				$("#metaDataVal" + counter).rules('add', {
					required: true, 
				    maxlength: 50
				}); 
			}
		}else{

			$("#metaDataKey" + counter).rules("remove", "required"); 
			// $("#metaDataUnit" + counter).rules("remove", "required"); 
			$("#metaDataType" + counter).rules("remove", "required"); 
			$("#metaDataVal" + counter).rules("remove", "required"); 

		}

	}); 
}

//If a value has been added but dependent fields aren't filled out (key, unit, type)
function datasetRequiredFields(){

		$.each($('.metaDataVal'), function(idx) {

			var currentId = (this.id);
			var counter = currentId.match(/\d+/); 
			var currentElementValue = $(this).val(); 

			if ($.trim(currentElementValue).length > 0) {

				$("#metaDataKey" + counter).rules('add', {
					required: true, 
				    maxlength: 50
				}); 

				// $("#metaDataUnit" + counter).each(function () {
				//     $(this).rules('add', {
				//         required: true, 
				//         maxlength: 50
				//     });
				// });

				$("#metaDataType" + counter).each(function () {
				    $(this).rules('add', {
				        required: true
				    });
				});	

				var type = $("#metaDataType" + counter + " option:selected").val(); 

				switch(type){
					case 'number':
						$(this).rules('add', {
							number: true, 
		 					maxlength: 20					
						}); 
					    $(this).rules('remove',"equals");
       					break;

					case 'boolean':
						$(this).rules('add', {
							equals: ["true", "false"]
						}); 
					    $(this).rules('remove',"number");
        				break;

					case 'string':
						$(this).rules('add', {
							maxlength: 50
						}); 
					    $(this).rules('remove',"number");
					    $(this).rules('remove',"equals");
       					break;
				}

			}
	});

}

$(document).on('click', '.createCollection', function(e){
    $("#formGetCollections").validate();
    if ($("#formGetCollections").valid()){
		postRootCollection(); 
    }
});

$(document).on('click', '.createSpace', function(e){
    $("#formGetSpaces").validate();
    if ($("#formGetSpaces").valid()){
		postSpace(); 
    }
});

function buildTemplate(idName) {
    var metaDataKeys = $.map($(idName + ' .metaDataKey'), function (el) { return el.value; });
    var metaDataVals = $.map($(idName + ' .metaDataVal'), function (el) {return el.value;});
    var metaDataUnits = $.map($(idName + ' .metaDataUnit'), function (el) {return el.value;});
    var metaDataTypes = $.map($(idName + ' .metaDataType'), function (el) {return el.value;});
    var requireField = $.map($(idName + ' .requireField'), function (el) {return el.value;});

    var arr = [];

    $.each(metaDataKeys, function (idx, keyName) {
        if (keyName != ''){
            var objCombined = {};
            objCombined['key'] = keyName;
            objCombined['units'] = metaDataUnits[idx];;
            objCombined['data_type'] = metaDataTypes[idx];;
            objCombined['default_value'] = metaDataVals[idx];
            objCombined['required'] = requireField[idx];

            arr.push(objCombined);
        }
    });
    return(arr);

}

// Auto complete dataset field
$(function() {

	var availableTags = [
		"Device Characterization",
		"Diffusion",
		"Ellipsometry",
		"Lithography",
		"Metallization",
		"Optical Microscopy",
		"Oxidation",
		"Plasma Etching",
		"Profilometry",
		"SEM",
		"SiO2 Mask Deposition",
		"SIMS",
		"SiNx Deposition",
		"SiNx Removal",
		"SPA"
	];

	$(".datasetName").autocomplete({
		source: availableTags
	});

	getAllTags(); 
	$(".tagData").hide(); 

});


//Auto create and remove textboxes for custom dataset settings
$(function () {
    $(".btnAdd").on('click', function () {
		var metaDataTags = [
			"Power",
			"Element",
			"Current", 
			"Pressure", 
			"Time", 
			"Temperature", 
			"Depth", 
			"Lateral Depth", 
			"Disorder Depth", 
			"Tool", 
			"Sample", 
			"Spin", 
			"RF", 
			"ICP",
			"SFP",
			"EBR", 
			"Expose", 
			"RIE", 
			"PostExp Bake",
			"Spin", 
			"PreExp Bake"
		];

        var div = $("<div />");
  		var menuName = $('.nav-tabs .active > a').attr("href");

        div.html(createDiv(" "));

        $(menuName + " .metaDataSettings").append(div);
		$(".metaDataKey").first().focus(); 
		$(".existingDS").show(); 
		$(".btnDataset").show();
		$("#btnTemplate").show();
		disableRequiredInput(); 

        //Call autocomplete on dynamically created textbox after it's created
		$(".metaDataKey").autocomplete({ 
			source:metaDataTags
 		});
 	});       

	$(document).on('click', 'body .remove', function(){
        $(this).closest(".top-buffer").remove();
    });

$(document).on('keypress', 'body', function(e){
	    if(e.which == 13){
	        $(".metaDataVal").blur(); 
	    }
	});    
});

//Search Plugin
$(document).on('keyup', '.search-input', function(){
	var searchString = $(this).val();
	$('#collections').jstree('search', searchString);
});

//Prevent collection search from submitting the form
$(document).on('keydown', '.search-input', function(e){
	if (e.keyCode == 13) {
		e.preventDefault();
	}
});

function counter() {
   var count = 0;

   this.reset = function() {
       count = 0;
       return count;
   };

   this.add = function() {
       return ++count;
   };
}

var counter1 = new counter();

//Create dynamic textbox
function createDiv(keyName, val, units, dataType, requireField) {
	console.log("CREATE DIV LARGER CALLED");
    var valKeyName = $.trim(keyName);
    var valStr = $.trim(val);
    var valUnits = $.trim(units);
    var valType = $.trim(dataType);
    var requireField = $.trim(requireField);

    if (requireField == ""){
    	requireField = "false";
    }

  	var menuName = $('.nav-tabs .active > a').attr("href");

  	//format text 
    var txtToWrite = "";

    if (menuName == "#createMenu"){
    	txtToWrite = "Optional Value:";
    }else{
    	txtToWrite = "Value: "
    }

	var i = counter1.add();    
    var txtString = dataType == "string" ? "<option value='string' selected>String</option>" : "<option value='string'>String</option>"; 
    var txtNumber = dataType == "number" ? "<option value='number' selected>Number</option>" : "<option value='number'>Number</option>"; 
    var txtBoolean = dataType == "boolean" ? "<option value='boolean' selected>Boolean</option>" : "<option value='boolean'>Boolean</option>"; 

    var txtTrue = requireField == "true" ? "<option value='true' selected>Yes</option>" : "<option value='true'>Yes</option>"; 
    var txtFalse = requireField == "false" ? "<option value='false' selected>No</option>" : "<option value='false'>No</option>"; 

    return '<div class="row top-buffer"><div class="col-xs-3"><b>' + "<label for='name'>Name: " + '</b></label>' +
        '<input class="metaDataKey form-control" name="metaDataKey' + i + '" id="metaDataKey' + i + '" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

        '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>"+ txtToWrite + '</b></label>' +
        '<input class="metaDataVal form-control" name="metaDataVal' + i + '" id="metaDataVal' + i + '" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +


        '<div class="col-xs-2"><b>' + "<label for='name'>Units: " + '</label></b>' +
        '<input class="metaDataUnit form-control" name="metaDataUnit' + i + '" id="metaDataUnit' + i + '" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div>' +

        '<div class="col-xs-2" style=""><b>' + "<label for='val'>Data Type: " + '</label></b>' +
        '<select class="metaDataType form-control" name="metaDataType' + i + '" id="metaDataType' + i + '" >' +
        '' + txtString + '' + 
        '' + txtNumber + '' + 
        '' + txtBoolean + '' + 
        '</select></div>' +


        '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>Required: " + '</label></b>' +
        '<select class="requireField form-control" name="requireField' + i + '" id="requireField' + i + '" >' +
        '' + txtTrue + '' + 
        '' + txtFalse + '' + 
        '</select></div>' +

        '<div class="col-xs-1" style="margin-left:-15px;"><b>' + "<label for='val'>&nbsp;" + '</label></b>' +
        '<input type="button" value="Remove" class="remove btn btn-danger btnRemove" name="btnRemove' + i + '" id="btnRemove' + i + '"></div></div>'
        
}

$.validator.addMethod("equals", function(value, element, string) {
    return $.inArray(value, string) !== -1;
}, $.validator.format("Please enter {0} or {1}"));



