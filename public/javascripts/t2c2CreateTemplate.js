$(document).ready(function() {

    $(".btnDataset").hide();
    $(".showTemplates").show();
    $(".showGlobalTemplates").show();
    $(".showTagTemplates").show();

    $('.checkShareTemplate').prop('checked', false);
    $(".templates").empty();
    $(".globalTemplates").empty();
    $(".tagTemplates").empty();
    $("#btnTemplate").hide();
    $("#updateTemplate .updatePanel").hide();     
    $('<option>').val('').text('--Select One--').appendTo('.templates');
    $('<option>').val('').text('--Select One--').appendTo('.globalTemplates');
    $('<option>').val('').text('--Select One--').appendTo('.tagTemplates');

    $(".tagData").hide();
    $(".templateSearch").val('');
    $(".metaDataSettings").empty();
    $(".templateData").empty();
    $('[data-toggle="popover"]').popover();
    getTemplates();
    getPublicTemplates();

});

//TEMPLATES
function getTemplates() {
    $.ajax({

        url: "listMyExperimentTemplates",
        type:"GET",
        dataType: "json",
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            showTemplates(data);
        },
        error: function(xhr, status, error) {
            swal({
                title: "Error",
                text: "There was a problem returning custom templates",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
        }
    })

}

function getPublicTemplates() {
    $.ajax({

        url: "getPublic",
        type:"GET",
        dataType: "json",
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            showGlobalTemplates(data);
        },
        error: function(xhr, status, error) {
            swal({
                title: "Error",
                text: "There was a problem returning custom templates",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
        }
    })

}

function getTemplate(id){
    $.ajax({

        url: "getExperimentTemplateById/"+id+"",
        type:"GET",
        dataType: "json",
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            createBoxes(data);
        },
        error: function(xhr, status, error) {
            swal({
                title: "Error",
                text: "There was a problem returning global templates",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
        }
    })
}

//Load user templates
function showTemplates(data) {
    $.each(data, function(key, val) {
        $(".templates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
    });

    $(".templates").focus();
}

//Load user templates
function showGlobalTemplates(templatesData) {
    $.each(templatesData, function(key, val) {
        $(".globalTemplates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
    });

    $(".globalTemplates").focus();
}

function showTagTemplates(data){
    $('<option>').val('').text('--Select One--').appendTo('.tagTemplates');
    $.each(data, function(key, val) {
        $(".tagTemplates").append($("<option class='placeholder'></option>").val(val.template_id).html(val.name));
    });

    $(".tagTemplates").focus();
}

function getAllTags(){

    $.ajax({
        url: "allTags",
        type:"GET",
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){

            //Format the object by removing whitespace, duplicates, and capitalize characters.
            var showUserTags = data.join(",");
            var allCapsTags = showUserTags.toUpperCase();
            var trimTags = $.map(allCapsTags.split(","), $.trim);
            var uniqueTags = jQuery.unique(trimTags);
            $(".templateSearch").autocomplete({
                source: uniqueTags,
                select: function (event, ui) {
                    $(".tagTemplates").empty();
                    $(".tagData").show();
                    $(".templateData").empty();
                    $(".metaDataSettings").empty();
                    $(".templates").val();
                    $(".globalTemplates").val();
                    var selectedObj = ui.item;
                    getByTagId(selectedObj.value);
                },
                change: function( event, ui ) {

                }
            });
        }

    });
}

function getByTagId(tagId){

    var url = "getIdNameFromTag/"+tagId+"";
    $.ajax({
        url: url,
        type:"GET",
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            $(".tagData").show();
            showTagTemplates(data);
        }

    });
}

function createBoxes(data){
    $(".btnDataset").show();
    $(".btnAdd").show();
    $("#btnTemplate").show();
    $(".updatePanel").show();

    //If active tab is update then fill in the name, tag fields
    var menuName = $('.nav-tabs .active > a').attr("href");

    if (menuName == "#updateTemplate"){
        $(".datasetName").val(data.name);
        $(".tagName").val(data.tags);

        if (data.isPublic == "true"){
            $('.checkShareTemplate').prop('checked', true);
        }
    }

    $.each(data.terms, function(key, val) {
        var div = $("<div />");
        div.html(createDiv(val.key, val.default_value, val.units, val.data_type, val.required));
        $(".templateData").append(div);
    });

}

function clearTemplate(){
    $(".templateData").empty();
    $(".metaDataSettings").empty();
    $(".btnDataset").hide();
    $("#btnTemplate").hide();
    $(".datasetName").val("");
    $(".tagName").val("");
    $(".templates").focus();
    $(".templates").val("");
    $(".globalTemplates").val("");
    $(".templateSearch").val("");
    $(".tagTemplates").val("");
    $(".tagData").hide();
    $(".checkShareTemplate").prop('checked', false);
    $(".updatePanel").hide(); 
}

$(document).on('click', '.clearTemplate', function(e){
    e.preventDefault();
    e.stopPropagation();
    clearTemplate();
});

$(document).on('click', '.updateTab, .createTab, .deleteTab', function(e){
    clearTemplate();
    var validator = $("#formCreateTemplate").validate();
    validator.resetForm();
    $("label.error").hide();
    $(".error").removeClass("error");
    counter1.reset();

});

//Handle template load when new menu item is selected
$(document).on('change', '.templates', function(){
    var id = $(this).val();
   
    if (id != ''){
        $(".globalTemplates").val("");
        $(".templateSearch").val("");
        $(".tagTemplates").val("");
        $(".templateData").empty();
        $(".metaDataSettings").empty();
        $(".tagData").hide();
        getTemplate(id);
    }
});

$(document).on('change', '.globalTemplates', function(){
    var id = $(this).val();

    if (id != ''){
        $(".templates").val("");
        $(".templateSearch").val("");
        $(".tagTemplates").val("");
        $(".templateData").empty();
        $(".metaDataSettings").empty();
        $(".tagData").hide();
        getTemplate(id);
    }
});

//If row is required
function datasetRequireAll(){

    var menuName = $('.nav-tabs .active > a').attr("href");

    $.each($(menuName + " " + '.requireField'), function(idx){

        var currentId = (this.id);
        var getIdFromInput = currentId.match(/\d+/);
        var counter = getIdFromInput[0];

        if ($(menuName + " " + "#requireField" + counter).val() == "true") {

            $(menuName + " " + "#metaDataKey" + counter).rules('add', {
                required: true,
                maxlength: 50
            });

            $(menuName + " " + "#metaDataUnit" + counter).rules('add', {
                required: true,
                maxlength: 50
            });

            $(menuName + " " + "#metaDataType" + counter).rules('add', {
                required: true,
                maxlength: 50
            });

        }else{

            $(menuName + " " + "#metaDataKey" + counter).rules("remove", "required");
            $(menuName + " " + "#metaDataUnit" + counter).rules("remove", "required");
            $(menuName + " " + "#metaDataType" + counter).rules("remove", "required");
            $(menuName + " " + "#metaDataVal" + counter).rules("remove", "required");

        }

    });
}

//If a value has been added but dependent fields aren't filled out (key, unit, type)
function datasetRequiredFields(){

    var menuName = $('.nav-tabs .active > a').attr("href");

    $.each($(menuName + " " + '.metaDataVal'), function(idx) {

        var currentId = (this.id);
        var getIdFromInput = currentId.match(/\d+/);
        var counter = getIdFromInput[0];
        var currentElementValue = $(this).val();

        if ($.trim(currentElementValue).length > 0) {
            $(menuName + " " + "#metaDataKey" + counter).rules('add', {
                required: true,
                maxlength: 50
            });

            $(menuName + " " + "#metaDataUnit" + counter).each(function () {
                $(this).rules('add', {
                    required: true,
                    maxlength: 50
                });
            });

            $(menuName + " " + "#metaDataType" + counter).each(function () {
                $(this).rules('add', {
                    required: true
                });
            });

            var type = $(menuName + " " + "#metaDataType" + counter).val();

            switch(type){
                case 'number':
                    $(menuName + " " + "#metaDataVal" + counter).rules('add', {
                        number: true,
                        maxlength: 20
                    });
                    $(menuName + " " + "#metaDataVal" + counter).rules('remove',"equals");
                    break;

                case 'boolean':
                    $(menuName + " " + "#metaDataVal" + counter).rules('add', {
                        equals: ["true", "false"]
                    });
                    $(menuName + " " + "#metaDataVal" + counter).rules('remove',"number");
                    break;

                case 'string':
                    $(menuName + " " + "#metaDataVal" + counter).rules('add', {
                        maxlength: 50
                    });
                    $(menuName + " " + "#metaDataVal" + counter).rules('remove',"number");
                    $(menuName + " " + "#metaDataVal" + counter).rules('remove',"equals");
                    break;
            }

        }
    });

}

// Create a new template
$(document).on('click', '.createTemplate', function(e){

    $("#formCreateTemplate").validate();

    datasetRequireAll();
    datasetRequiredFields();

    if ($("#formCreateTemplate").valid()){
        postTemplate(e,"#formCreateTemplate");
    }

});

//Update a template
$(document).on('click', '.updateTemplate', function(e){

    $("#formUpdateTemplate").validate();

    datasetRequireAll();
    datasetRequiredFields();

    var id = $("#formUpdateTemplate .templates").val();
    if ($("#formUpdateTemplate").valid()){
        updateTemplate(id,"#formUpdateTemplate");
    }

});

// Delete a template
$(document).on('click', '.deleteTemplate', function(e){
    if ($("#formDeleteTemplate").valid()){
        var id = $("#formDeleteTemplate .templates").val(); 
        deleteTemplate(id);
    }
});

//Posts new template
function postTemplate(e,className) {
    e.preventDefault();
    e.stopPropagation();

    var templateTerms = buildTemplate(className);
    var templateName = $('.datasetName').val();
    var tagName = $('.tagName').val().toUpperCase();
    var shareTemplate = $('.checkShareTemplate').is(":checked");
    $.ajax({
        url: "createExperimentTemplateFromJson?isPublic="+shareTemplate+"",
        type:"POST",
        data: JSON.stringify({ name: templateName, terms: templateTerms, tags: tagName}),
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            swal({
                title: "Success",
                text: "A new template was created",
                type: "success",
                timer: 1500,
                showConfirmButton: false
            });

            // clear all the inputs in the new dataset field tabs
            $(".templateData").empty();
            $(".metaDataSettings").empty();
            $(".templates").empty();
            $(".globalTemplates").empty();
            $("#btnTemplate").hide();
            $(".datasetName").val('');
            $(".tagName").val('');
            $('.checkShareTemplate').prop('checked', false);
            $('<option>').val('').text('--Select One--').appendTo('.templates');
            $('<option>').val('').text('--Select One--').appendTo('.globalTemplates');
            $('<option>').val('').text('--Select One--').appendTo('.tagTemplates');

            getTemplates();
            getPublicTemplates();
        },
        error: function(xhr, status, error) {
            swal({
                title: "Error",
                text: "There was a problem creating this template",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
        }
    })
}

function updateTemplate(id, className){

    var templateTerms = buildTemplate(className);
    var templateName = $('#formUpdateTemplate .datasetName').val();
    var tagName = $('#formUpdateTemplate .tagName').val().toUpperCase();
    var shareTemplate = $('#formUpdateTemplate .checkShareTemplate').is(":checked").toString();

    $.ajax({
        url: "editTemplate/"+id+"",
        type:"PUT",
        data: JSON.stringify({ name: templateName, terms: templateTerms, tags: tagName, isPublic : shareTemplate}),
        beforeSend: function(xhr){
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(data){
            swal({
                title: "Success",
                text: "Your template was edited",
                type: "success",
                timer: 1500,
                showConfirmButton: false
            });

            // clear all the inputs in the new dataset field tabs
            $(".templateData").empty();
            $(".metaDataSettings").empty();
            $(".templates").empty();
            $(".globalTemplates").empty();
            $("#btnTemplate").hide();
            $(".datasetName").val('');
            $(".tagName").val('');
            $('.checkShareTemplate').prop('checked', false);
            $('<option>').val('').text('--Select One--').appendTo('.templates');
            $('<option>').val('').text('--Select One--').appendTo('.globalTemplates');
            $('<option>').val('').text('--Select One--').appendTo('.tagTemplates');
            $(".updatePanel").hide();
            getTemplates();
            getPublicTemplates();
        },
        error: function(xhr, status, error) {
            swal({
                title: "Error",
                text: "There was a problem updating this template",
                type: "error",
                timer: 1500,
                showConfirmButton: false
            });
        }
    })     

}

function deleteTemplate(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this template.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: "deleteTemplate/"+id+"", 
            type:"DELETE",
            success: function(data){

                swal({
                    title: "Success",
                    text: "Your template was deleted",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                // clear all the inputs in the new dataset field tabs
                $(".templateData").empty();
                $(".metaDataSettings").empty();
                $(".templates").empty();
                $(".globalTemplates").empty();
                $("#btnTemplate").hide();
                $(".datasetName").val('');
                $(".tagName").val('');
                $('.checkShareTemplate').prop('checked', false);
                $('<option>').val('').text('--Select One--').appendTo('.templates');
                $('<option>').val('').text('--Select One--').appendTo('.globalTemplates');
                $('<option>').val('').text('--Select One--').appendTo('.tagTemplates');

                getTemplates();
                getPublicTemplates();
            },
            error: function(xhr, status, error) {
                swal({
                    title: "Error",
                    text: "There was a problem deleting this template",
                    type: "error",
                    timer: 1500,
                    showConfirmButton: false
                });
            }

        })

    });
}

function buildTemplate(idName) {
    var metaDataKeys = $.map($(idName + ' .metaDataKey'), function (el) { return el.value; });
    var metaDataVals = $.map($(idName + ' .metaDataVal'), function (el) {return el.value;});
    var metaDataUnits = $.map($(idName + ' .metaDataUnit'), function (el) {return el.value;});
    var metaDataTypes = $.map($(idName + ' .metaDataType'), function (el) {return el.value;});
    var requireField = $.map($(idName + ' .requireField'), function (el) {return el.value;});

    var arr = [];

    $.each(metaDataKeys, function (idx, keyName) {
        if (keyName != ''){
            var objCombined = {};
            objCombined['key'] = keyName;
            objCombined['units'] = metaDataUnits[idx];;
            objCombined['data_type'] = metaDataTypes[idx];;
            objCombined['default_value'] = metaDataVals[idx];
            objCombined['required'] = requireField[idx];

            arr.push(objCombined);
        }
    });

    return(arr);

}

//Auto complete dataset field
$(function() {

    //Change these to whatever...
    var availableTags = [
        "Device Characterization",
        "Diffusion",
        "Ellipsometry",
        "Lithography",
        "Metallization",
        "Optical Microscopy",
        "Oxidation",
        "Plasma Etching",
        "Profilometry",
        "SEM",
        "SiO2 Mask Deposition",
        "SIMS",
        "SiNx Deposition",
        "SiNx Removal",
        "SPA"
    ];

    $(".datasetName").autocomplete({
        source: availableTags
    });

    getAllTags();

});

//Auto create and remove textboxes for custom dataset settings
$(function () {
    $(".btnAdd").on('click', function () {
        var metaDataTags = [
            "Power",
            "Element",
            "Current",
            "Pressure",
            "Time",
            "Temperature",
            "Depth",
            "Lateral Depth",
            "Disorder Depth",
            "Tool",
            "Sample",
            "Spin",
            "RF",
            "ICP",
            "SFP",
            "EBR",
            "Expose",
            "RIE",
            "PostExp Bake",
            "Spin",
            "PreExp Bake"
        ];

        var div = $("<div />");
        div.html(createDiv(" "));

        $(".metaDataSettings").append(div);
        $(".metaDataKey").first().focus();
        $(".btnDataset").show();
        $("#btnTemplate").show();

        //Call autocomplete on dynamically created textbox after it's created
        $(".metaDataKey").autocomplete({
            source:metaDataTags
        });
    });

    $("body").on("click", ".remove", function () {
        $(this).closest(".top-buffer").remove();
    });

    $("body").keypress(function(e){
        if(e.which == 13){
            $(".metaDataVal").blur();
        }
    });
});


function counter() {
   var count = 0;

   this.reset = function() {
       count = 0;
       return count;
   };

   this.add = function() {
       return ++count;
   };
}

var counter1 = new counter();

//Create dynamic textbox
function createDiv(keyName, val, units, dataType, requireField) {
    var valKeyName = $.trim(keyName);
    var valStr = $.trim(val);
    var valUnits = $.trim(units);
    var valType = $.trim(dataType);
    var requireField = $.trim(requireField);

    if (requireField == ""){
        requireField = "false";
    }
    var menuName = $('.nav-tabs .active > a').attr("href");

    //format text
    var txtToWrite = "";

    if (menuName == "#createMenu"){
        txtToWrite = "Optional Value:";
    }else{
        txtToWrite = "Value: "
    }

    var i = counter1.add();
    var txtString = dataType == "string" ? "<option value='string' selected>String</option>" : "<option value='string'>String</option>";
    var txtNumber = dataType == "number" ? "<option value='number' selected>Number</option>" : "<option value='number'>Number</option>";
    var txtBoolean = dataType == "boolean" ? "<option value='boolean' selected>Boolean</option>" : "<option value='boolean'>Boolean</option>";

    var txtTrue = requireField == "true" ? "<option value='true' selected>Yes</option>" : "<option value='true'>Yes</option>";
    var txtFalse = requireField == "false" ? "<option value='false' selected>No</option>" : "<option value='false'>No</option>";

    return '<div class="row top-buffer"><div class="col-xs-3"><b>' + "<label for='name'>Name: " + '</b></label>' +
        '<input class="metaDataKey form-control" name="metaDataKey' + i + '" id="metaDataKey' + i + '" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

        '<div class="col-xs-2"><b>' + "<label for='name'>Unit Type: " + '</label></b>' +
        '<input class="metaDataUnit form-control" name="metaDataUnit' + i + '" id="metaDataUnit' + i + '" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div>' +

        '<div class="col-xs-2" style=""><b>' + "<label for='val'>Data Type: " + '</label></b>' +
        '<select class="metaDataType form-control" name="metaDataType' + i + '" id="metaDataType' + i + '" >' +
        '' + txtString + '' +
        '' + txtNumber + '' +
        '' + txtBoolean + '' +
        '</select></div>' +

        '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>"+ txtToWrite + '</b></label>' +
        '<input class="metaDataVal form-control" name="metaDataVal' + i + '" id="metaDataVal' + i + '" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +

        '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>Required: " + '</label></b>' +
        '<select class="requireField form-control" name="requireField' + i + '" id="requireField' + i + '" >' +
        '' + txtTrue + '' +
        '' + txtFalse + '' +
        '</select></div>' +

        '<div class="col-xs-1" style="margin-left:-15px;"><b>' + "<label for='val'>&nbsp;" + '</label></b>' +
        '<input type="button" value="Remove" class="remove btn btn-danger btnRemove" name="btnRemove' + i + '" id="btnRemove' + i + '"></div></div>'

}

$.validator.addMethod("equals", function(value, element, string) {
    return $.inArray(value, string) !== -1;
}, $.validator.format("Please enter {0} or {1}"));





